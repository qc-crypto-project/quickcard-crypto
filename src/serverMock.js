// src/server.js
import { createServer, Response } from 'miragejs';
import axios from 'axios';

import userList from './userList';

function authenticate(request) {
  let authenticated = null;
  userList.forEach((storedUser) => {
    if (request.login === storedUser.login && request.password === storedUser.password) {
      authenticated = storedUser;
    }
  });
  return authenticated;
}

//store create-account data
const accountData = {
  firstName: '',
  lastName: '',
  phoneData: '',
  phoneNumber: '',
  email: '',
  phoneVerification: '',
  emailVerification: '',
};

//trigger verification email delivery
const sendMail = async () => {
  const data = {
    service_id: 'service_x6fec4o',
    template_id: 'template_6jjtjlq',
    user_id: 'user_zUHQOaB4YxhDUCcReM1LK',
    template_params: {
      to_name: `${accountData.firstName} ${accountData.lastName}`,
      code: accountData.emailVerification,
      destination: accountData.email,
    },
  };
  const response = await axios.post('https://api.emailjs.com/api/v1.0/email/send', data);
  console.log(response);
};

// writes accountData
const storeUserData = (attrs) => {
  accountData.lastName = attrs.lastName;
  accountData.firstName = attrs.firstName;
  accountData.phoneData = attrs.phoneData;
  accountData.phoneNumber = attrs.phoneNumber;
  accountData.email = attrs.email;
  accountData.phoneVerification = (100000 + Math.floor(Math.random() * 900000)).toString();
  accountData.emailVerification = (100000 + Math.floor(Math.random() * 900000)).toString();
};

export default function () {
  createServer({
    routes() {
      this.timing = 1500;

      this.post('/api/authenticate', (schema, request) => {
        let attrs = JSON.parse(request.requestBody);
        if (authenticate(attrs)) {
          return {
            userData: authenticate(attrs),
            token: 'jkilogjdf09gjAOPIJd.0A9UDPoadJUADPO09',
          };
        } else {
          return new Response(401, { header: 'header' }, { errors: ['Invalid user or password'] });
        }
      });

      this.post('/api/phone-verification', (schema, request) => {
        let attrs = JSON.parse(request.requestBody);
        if (attrs.code === '123456') {
          return {
            success: true,
          };
        } else {
          return { error: 'Invalid code' };
        }
      });

      this.post('/api/email-verification', (schema, request) => {
        let attrs = JSON.parse(request.requestBody);
        if (attrs.code === accountData.emailVerification) {
          return {
            success: true,
          };
        } else {
          return { error: 'Invalid code' };
        }
      });

      this.post('/api/create-user', (schema, request) => {
        let attrs = JSON.parse(request.requestBody);
        if (attrs.phoneNumber !== '123456789') {
          storeUserData(attrs);
          console.log(accountData);
          sendMail();
          return { success: true };
        } else {
          return new Response(401, { header: 'header' }, { errors: ['Phone number already used'] });
        }
      });

      this.get('/api/user/admin', () => {
        return {
          user: `${accountData.firstName} ${accountData.lastName}`,
          account: 'C-987654321',
          balance: 428.348,
        };
      });
      this.passthrough('https://ipv4.icanhazip.com/');
      this.passthrough('https://ipv4.icanhazip.com/**');
      this.passthrough('https://api.emailjs.com/**');
    },
  });
}

import React, {useState} from 'react';

const PaymentContext = React.createContext(null);

const Context = ({children}) => {
  const [paymentInfos, setPaymentInfos] = useState();

  return (
    <PaymentContext.Provider value={{paymentInfos, setPaymentInfos}}>
      {children}
    </PaymentContext.Provider>
  )
}

export {Context};
export default PaymentContext;
import React, { useState, createContext, useContext } from 'react';

const CreateAccountContext = createContext(null);

export default function CreateAccountProvider({ children }) {
  const [phone, setPhone] = useState({ iso2: 'us', dialCode: '1', phone: '' });
  const [email, setEmail] = useState(null);
  const [userData, setUserData] = useState({
    firstName: '',
    lastName: '',
    phoneData: '',
    phoneNumber: '',
    email: '',
  });

  const updateUserData = (value) => {
    setUserData((prev) => ({ ...prev, ...value }));
  };

  return (
    <CreateAccountContext.Provider
      value={{ phone, setPhone, email, setEmail, userData, updateUserData }}
    >
      {children}
    </CreateAccountContext.Provider>
  );
}

export const useCreateAccount = () => {
  const context = useContext(CreateAccountContext);
  if (!context) {
    throw new Error('UseCreateAccount must be called within CreateAccountProvider');
  }
  return context;
};

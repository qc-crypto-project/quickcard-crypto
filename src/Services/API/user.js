import { Client } from "./client"
const client = new Client();
export class User {

    login = (url, data, headers = {}) => {
      return client.post(url, data);
    };
  
    getUsers = (url) => {
      return client.get(url); 
    };
  }
  export default User
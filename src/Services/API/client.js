import axios from "axios";
import { postHeaders, config } from "../../constants";
const baseUrl = config.apiServerURL
const postHeader = postHeaders
export class Client {

  setJwt = (jwt) => {
    // this.externalClient.setJwt(jwt);
  };

  post = (url, data, headers = {}) => {
    return axios.post(`${baseUrl}${url}`, data, {postHeader}).catch(function (error) {
      return error.response;
    });
  };

  get = (url) => {
    let auth_token = `Bearer ${localStorage.getItem("token")}`
    return axios.get(`${baseUrl}${url}`, {headers: {Authorization: auth_token}});
  };

  // login = (url, data) => {
  //   return axios_post(
  //     url,
  //     data
  //   );
  // };

  createUser = (
    fullName,
    email,
    password,
    agreements
  ) => {
    // return this.externalClient.user.create(
    //   fullName,
    //   email,
    //   agreements,
    //   password
    // );
  };

  updateUser = (userSetting) => {
    // this.externalClient.user.updateUser(userSetting);
  };
}
export default Client

import axios from 'axios';

const checkoutApi = axios.create({ baseURL: 'api' });

checkoutApi.interceptors.request.use((config) => {
  const jwt = localStorage.getItem('jwt');
  if (!config.url.endsWith('login') && !config.url.endsWith('refresh')) {
    if (jwt) {
      return { headers: { Authorization: `Bearer ${jwt}` } };
    }
  }
  return config;
});

export default checkoutApi;

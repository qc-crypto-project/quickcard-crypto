import isEmail from 'validator/lib/isEmail';

/**
 * A collection of static input validation methods
 */
class Validator {
  /**
   * Validate an email address
   */
  static email(email) {
    if (typeof email !== 'string') {
      return false;
    }

    return isEmail(email);
  }

  static constHideEmail(email) {
    const censorWord = function (str, number) {
      return str[0] + '*'.repeat(str.length - 2) + str.slice(-number);
    };

    const arr = email.split('@');
    return censorWord(arr[0], 2) + '@' + censorWord(arr[1], 4);
  }

  /**
   * Validate a name
   */
  static isName(name) {
    if (typeof name !== 'string') {
      return false;
    }

    return name.length !== 0;
  }

  /**
   * Validate a password
   */
  static password(password) {
    if (typeof password !== 'string') {
      return false;
    }

    return password.length !== 0;
  }
}

export { Validator };

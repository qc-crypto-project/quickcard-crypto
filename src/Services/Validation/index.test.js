import { Validator } from './index';

describe('Validator class', () => {
  describe('email validation', () => {
    it('passes with valid email', () => {
      expect(Validator.email('test@example.com')).toBe(true);
    });

    it('passes with valid emails with non-standard TLDs', () => {
      expect(Validator.email('test@example.edu')).toBe(true);
      expect(Validator.email('test@example.io')).toBe(true);
      expect(Validator.email('test@example.co.uk')).toBe(true);
      expect(Validator.email('test@example.gov')).toBe(true);
    });

    it('fails with invalid email', () => {
      expect(Validator.email('')).toBe(false);
      expect(Validator.email('no-tld@example')).toBe(false);
      expect(Validator.email('no-at-symbol.example.com')).toBe(false);
      expect(Validator.email('includes space@example.com')).toBe(false);
    });

    it('fails with non-string email', () => {
      expect(Validator.email(NaN)).toBe(false);
      expect(Validator.email(undefined)).toBe(false);
      expect(Validator.email(123)).toBe(false);
      expect(Validator.email({ valid: false })).toBe(false);
    });
  });

  describe('name validation', () => {
    it('passes with single-word name', () => {
      expect(Validator.isName('First')).toBe(true);
    });

    it('passes with multi-word name', () => {
      expect(Validator.isName('First Second')).toBe(true);
      expect(Validator.isName('First Second Third')).toBe(true);
      expect(Validator.isName('First Second III')).toBe(true);
    });

    it('passes with name containing non-alphabetic characters', () => {
      expect(Validator.isName("First O'second")).toBe(true);
      expect(Validator.isName('First Second-Third')).toBe(true);
      expect(Validator.isName('Mr. First Second')).toBe(true);
      expect(Validator.isName('First Second, D.D.S.')).toBe(true);
    });

    it('fails with length-zero name', () => {
      expect(Validator.isName('')).toBe(false);
    });

    it('fails with non-string name', () => {
      expect(Validator.isName(NaN)).toBe(false);
      expect(Validator.isName(undefined)).toBe(false);
      expect(Validator.isName(123)).toBe(false);
      expect(Validator.isName({ valid: false })).toBe(false);
    });
  });

  describe('password validation', () => {
    it('passes with length non-zero password', () => {
      expect(Validator.password('a')).toBe(true);
      expect(Validator.password('abcdefg')).toBe(true);
      expect(Validator.password('abcdefg'.repeat(10))).toBe(true);
    });

    it('passes with password containing only alphanumeric characters', () => {
      expect(Validator.password('abc123')).toBe(true);
    });

    it('passes with password containing mix of alphanumeric and non-alphanumeric characters', () => {
      expect(Validator.password("abc123'-!@#$%^&*(){}[]_-+=")).toBe(true);
    });

    it('fails with length-zero password', () => {
      expect(Validator.password('')).toBe(false);
    });

    it('fails with non-string password', () => {
      expect(Validator.password(NaN)).toBe(false);
      expect(Validator.password(undefined)).toBe(false);
      expect(Validator.password(123)).toBe(false);
      expect(Validator.password({ valid: false })).toBe(false);
    });
  });
});

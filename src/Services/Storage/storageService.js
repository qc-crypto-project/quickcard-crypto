const StorageKeys = {
  USER_TOKEN: 'app/user/token',
  USER: 'app/user',
  TOKEN: 'token',
  USER_EMAIL: 'user-email',
  USER_PHONE: 'user-phone',
};

export class StorageService {
  static get(key) {
    localStorage.getItem(StorageKeys[key]);
  }

  static set(key, value) {
    localStorage.setItem(StorageKeys[key], value);
  }

  static clear(key) {
    localStorage.removeItem(StorageKeys[key]);
  }

  static parseGet(key) {
    JSON.parse(localStorage.getItem(StorageKeys[key]));
  }

  static stringSet(key, value) {
    localStorage.setItem(StorageKeys[key], JSON.stringify(value));
  }
}

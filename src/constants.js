import * as dotenv from "dotenv";
import * as path from "path";

const envPath = (process.env.NODE_ENV === "production") ? ".env.prod" : ".env";

console.log("dirqq", __dirname)
try {
  dotenv.config({ path: "../.env"});
  console.log(`../${ envPath }`);
  console.log(`qc-crypto: Loading parameters: ${envPath}`);
} catch (error) {
  console.log(`qc-crypto: ${envPath} file is missing, using env variables.`);
}

function fromEnv(name, required = true) {
  if (required && !process.env[name]) {
    throw new Error(`Required Environment variable ${name} does not exist.`);
  }
  return process.env[name];
}

function parseEnv(name, value = true) {
  const variable = process.env[name];
  return variable ?  JSON.parse(variable) : value;
}

export const postHeaders = {
  headers: {
    'Accept': 'application/json',
    'Content-Type': 'application/json'
  }
}

export const authorization = {
    Authorization: `Bearer ${localStorage.getItem("token")}`
}

export const config = {
  apiServerURL: fromEnv("REACT_APP_API_SERVER_URL")
};

export const getResponse = (response) => {
  var responseMessage = {};
  var reponseTitle ;
  if (response.status === 401){
    reponseTitle = "Unautherized"
  }else if(response.status === 500){
    reponseTitle =  "Server Error"
  }else if(response.status === 404){
    reponseTitle =  "Page Not Found"
  }else{
    reponseTitle =  "Not Found"
  }
  responseMessage = {
    title: reponseTitle,
    icon: "warning",
    text: response.data.message
  }
  return responseMessage;
}
import React from "react";
import { Route } from "react-router-dom";
import Login from "./Components/Login";
import SignUp from "./Components/SignUp";
// import LogIn from "./LogIn";
import { PrivateRoute } from "./PrivateRoute";
import { PublicRoute } from "./PublicRoute";
// import Dashboard from "./Components/Dashboard";
import SideDrawer from "./Components/SideDrawer";
import LoginDemo from "./Login-demo";

export const RootRoute = (props) => {
  const { match } = props;
  return (
    <>
      <PublicRoute
        exact
        path={`${match.url}`}
        component={Login}
      />
      {/* <Route exact path={`${match.url}login`} component={Login} /> */}
      <Route exact path={`${match.url}login`} component={LoginDemo} />
      <Route exact path={`${match.url}signup`} component={SignUp} />
      <PrivateRoute
        exact
        path={`${match.url}dashboard`}
        component={SideDrawer}
      />
      <PrivateRoute
        exact
        path={`${match.url}users`}
        component={SideDrawer}
      />
    </>
  );
};

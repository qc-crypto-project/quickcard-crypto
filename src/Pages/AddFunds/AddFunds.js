import React from 'react';

import { CardRound } from '../../Components/';
import PaymentForm from '../../Components/PaymentForm/PaymentForm';
import { InternalSimple, Footer } from '../../Components/Layouts';
import InternalHeader from '../../Components/InternalHeader/InternalHeader';

function AddFunds() {
  const getInfos = {
    currency: 'QUSD',
    value: '500.00',
  };

  const payInfos = {
    currency: 'GBP',
    value: '520.00',
  };

  return (
    <>
      <InternalSimple styleName="add-funds-page" padding={false}>
        <InternalHeader />
        <div className="add-funds">
          <div className="add-funds__form">
            <PaymentForm />
          </div>
          <div className="add-funds__currency-box">
            <CardRound variation="card" get={getInfos} pay={payInfos} />
          </div>
        </div>
      </InternalSimple>
    </>
  );
}

export default AddFunds;

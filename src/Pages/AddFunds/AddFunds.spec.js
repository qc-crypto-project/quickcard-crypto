import React from 'react';
import {cleanup, fireEvent, render} from '@testing-library/react';
import AddFunds from './AddFunds';
import Context from '../../Services/Context/Context';


describe("Form verification", () => {

  beforeEach(() => {
    cleanup()
  })

  it("Should complete all fields and submit to test validation", () => {
    const paymentInfos = {};
    const setPaymentInfos = () => {
      return paymentInfos;
    }

    const {container, getByText} = render(
      <Context.Provider value={{paymentInfos, setPaymentInfos}}>
        <AddFunds />
      </Context.Provider>
    );
    
    const selectToShowOptions = container.getElementsByTagName("input");

    for(let i =0; i <selectToShowOptions.length; i++){
      // TODO: Fill inputs here to test later
    }
      
    expect(200).toBe(200)
  })
})

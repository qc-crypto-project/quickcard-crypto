import React, { useState, useEffect } from 'react';

import InternalSimple from '@components/Layouts/InternalSimple';
import CardRound from '@components/CardRound/CardRound';
import CardSelect from '@components/CardSelect/CardSelect';
import Footer from '@components/Layouts/Footer';
import Loading from '@components/Loading/Loading';
import RemoveCard from '@components/RemoveCard/RemoveCard';
import RemoveCardConfirmation from '@components/RemoveCardConfirmation/RemoveCardConfirmation';
import EditCardModal from '@components/EditCardModal/EditCardModal';

import { Label } from 'greenbox_ui/src/components';
import { Button } from 'greenbox_ui/src/components';

const Cards = () => {
  const [isloading, setIsLoading] = useState(true)
  const [userCards, setUserCards] = useState([])
  const [payInfos, setPayInfos] = useState();
  const [getInfos, setGetInfos] = useState();
  const [showRemovedCardModal, setShowRemovedCardModal] = useState();
  const [showEditCardModal, setShowEditCardModal] = useState();
  const [showRemoveCard, setShowRemoveCard] = useState()
  const [cardIdToRemove, setCardIdToRemove] = useState();
  const [cardInfos, setCardInfos] = useState();

  useEffect(() => {
    // Put api to get cards here
    const simulateInfosOfCards1 = {
      id: Math.random(),
      cardInfos: {
        nameCard: 'John smith',
        cardNumber: '5290',
        expireDate: {
          mounth: 11,
          year: 2020,
        },
      },
      userInfos: {
        name: 'John smith',
        address1: 'M8 8LG, 165, Cheetham Hill',
        address2: 'M8 8LG, 165, Cheetham Hill',
        country: 'UK',
        state: 'Manchester',
        city: 'Manchester',
        zip: '010',
      },
    };

    const simulateInfosOfCards2 = {
      id: Math.random(),
      cardInfos: {
        nameCard: 'John smith',
        cardNumber: '5290',
        expireDate: {
          mounth: 11,
          year: 2019,
        },
      },
      userInfos: {
        name: 'John smith',
        address1: 'M8 8LG, 165, Cheetham Hill',
        address2: 'M8 8LG, 165, Cheetham Hill',
        country: 'UK',
        state: 'Manchester',
        city: 'Manchester',
        zip: '010',
      },
    };
    const simulateCards = [
      simulateInfosOfCards1,
      simulateInfosOfCards2,
      simulateInfosOfCards1,
      simulateInfosOfCards2,
    ];

    setUserCards((prev) => [...prev, ...simulateCards]);

    setPayInfos({
      value: '420.00',
      currency: 'GBP',
    });

    setGetInfos({
      value: '400.00',
      currency: 'QUSD',
    });

    setIsLoading(false)
  }, [])

  const handleConfirmationRemoveCard = (cardId) => {
    setShowRemoveCard(true)
    setCardIdToRemove(cardId)
  }

  const handleVerifyRemoved = (removed) => {
    console.log(removed)
    
    if(removed){
      setShowRemoveCard(false)
      setShowRemovedCardModal(true)

    }
  };

  const handleEditCard = (cardInfos) => {
    setShowEditCardModal(true)
    setCardInfos(cardInfos)
  }

  return isloading ? 
    <Loading />
    :
    <>
      <InternalSimple>
        {
          showEditCardModal && cardInfos ? 
                <EditCardModal 
                  card={cardInfos} 
                  onClose={() => setShowEditCardModal(false)}
                  onEdit={() => console.log("Show toast modal")}
                /> 
            :
            null
        }
        {
          showRemoveCard && cardIdToRemove ?
            <RemoveCard 
              cardId={cardIdToRemove} 
              onRemove={handleVerifyRemoved} 
              onClose={() => setShowRemoveCard(false)}
              onClickNo={() => window.location.reload()} 
            /> 
            :
            null
        }
        {
          showRemovedCardModal ?
            <RemoveCardConfirmation 
              onClose={() => setShowRemovedCardModal(false)}
            />
            :
            null
        }
        <div className="cards-container">
          <div className="cards-container__cards">
            <Label variation="black"> Select Card </Label>
            {
              userCards.map(card => (
                <CardSelect 
                  cardInfos={card} 
                  onRemove={() => handleConfirmationRemoveCard(card.id)}
                  onEdit={() => handleEditCard(card)}
                />
              ))
            }
            <div className="cards-container__cards__buttons">
              <div className="cards-container__cards__buttons--new-card">
                <Button variation="secondary">
                  <span className="more-icon"> + </span> New Card  
                </Button>
              </div>
            </div>
          </div>
          <div className="cards-container__card-round">
            <CardRound pay={payInfos} get={getInfos} />
          </div>
        </div>
      </InternalSimple>
      <Footer />
    </>
}

export default Cards;

import React, { useEffect, useState } from "react";
import {
    makeStyles,
    Paper,
    Grid,
    TextField,
    Table,
    TableBody,
    TableCell,
    TableContainer,
    TableHead,
    TablePagination,
    TableRow,
    Backdrop,
    CircularProgress,
} from "@material-ui/core";
import { Search } from "@material-ui/icons";

import { API_ROUTES } from "../Routes/apiRoutes";
import { User } from "../Services/API/user";
import swal from "sweetalert";
import { StorageService } from "../Services/Storage/storageService"

const head = ["Merchant ID", "First Name", "Last Name", "Email", "Phone", "Status"];
const rowsPageNo = [10, 25, 50, 100];
const users = new User();

export default function DashboardTable() {
    const classes = useStyles();
    const [page, setPage] = useState(0);
    const [rowsPerPage, setRowsPerPage] = useState(10);
    const [search, setSearch] = useState("");
    const [data, setData] = useState([]);
    const [count, setCount] = useState(0);
    const [open, setOpen] = useState(false);

    const fetchData = () => {
        setOpen(true);
        const data = {
            page: page + 1,
            per_page: rowsPerPage,
            sort: { name: "created_at", type: "desc" },
        };
        users.getUsers(API_ROUTES.users)
            .then((res) => {
                setData(res?.data?.users);
                setCount(res?.data?.users.total_count);
                setOpen(false);
            }).catch((error) => {
                swal({
                    title: "Error Message",
                    icon: "warning",
                    text: error,
                });
          setOpen(false);
        });
    };

    useEffect(() => {
        fetchData();
    }, [page, rowsPerPage]);

    const handleChangePage = (event, newPage) => {
        setPage(newPage);
    };

    const handleChangeRowsPerPage = (event) => {
        setRowsPerPage(parseInt(event.target.value));
    };

    // useEffect(() => {
    //     const timeOutId = setTimeout(() => searchData(), 1500);
    //     return () => clearTimeout(timeOutId);
    // }, [search]);

    const updateSearch = (event) => {
      setSearch(event.target.value.substr(0, 20));
    };
    const filterName = (item) => {
      {
        if (!search) return true;
        if (item.first_name.toLowerCase().includes(search.toLowerCase())) {
          return true;
        }
      }
    };

    const searchData = (force = false) => {
        // if (search.length < 3 && !force) {
        //   return;
        // }
        setOpen(true);
        const token = StorageService.get("TOKEN");
        const config = {
            headers: {
                Authorization: `${token}`,
            },
        };
        const data = {
            search: {
                value: search,
            },
            per_page: rowsPerPage,
            sort: { name: "created_at", type: "desc" },
        };
        // axios
        //     .post(API_ROUTES.users_url, data, config)
        //     .then((res) => {
        //         setData(res?.data?.users?.data);
        //         setOpen(false);
        //         // setSearch("");
        //     })
        //     .catch((error) => {
                // swal({
                //     title: "Error Message",
                //     icon: "warning",
                //     text: error,
                // });
// 
                // setOpen(false);
            // });
    };

    return (
        <>
            <Paper className={classes.root}>
                <Grid container>
                    <Grid item xs={12} sm={6} md={6} lg={6} xl={6}>
                        <TextField
                            label="Search"
                            variant="outlined"
                            size="small"
                            value={search}
                            onChange={(e) => setSearch(e.target.value)}
                        />
                    </Grid>
                    <Grid item xs={12} sm={6} md={6} lg={6} xl={6}>
                        <TablePagination
                            rowsPerPageOptions={rowsPageNo}
                            component="div"
                            count={count}
                            rowsPerPage={rowsPerPage}
                            page={page}
                            onChangePage={handleChangePage}
                            onChangeRowsPerPage={handleChangeRowsPerPage}
                        />
                    </Grid>
                </Grid>
                <TableContainer className={classes.container}>
                    <Table stickyHeader>
                        <TableHead>
                            <TableRow>
                                {head.map((column) => (
                                    <TableCell key={column}>{column}</TableCell>
                                ))}
                            </TableRow>
                        </TableHead>
                        <TableBody>
                            {data
                                // ?.filter((item) => filterName(item))
                                ?.map((row, index) => (
                                    <TableRow hover role="checkbox" tabIndex={-1} key={row}>
                                        <TableCell key={`${index}-${row.id}`}>{row.id}</TableCell>
                                        <TableCell key={`${index}-${row.first_name}`}>
                                            {row.first_name}
                                        </TableCell>
                                        <TableCell key={`${index}-${row.last_name}`}>
                                            {row.last_name}
                                        </TableCell>
                                        <TableCell key={`${index}-${row.email}`}>
                                            {row.email}
                                        </TableCell>
                                        <TableCell key={`${index}-${row.phone_number}`}>
                                            {row.phone_number}
                                        </TableCell>
                                        <TableCell key={`${index}-${row.status}`}>
                                            {row.status}
                                        </TableCell>
                                    </TableRow>
                                ))}
                        </TableBody>
                    </Table>
                </TableContainer>
            </Paper>
            <Backdrop className={classes.backdrop} open={open}>
                <CircularProgress color="primary" />
            </Backdrop>
        </>
    );
}

const useStyles = makeStyles((theme) => ({
    root: {
        width: "100%",
    },
    container: {
        maxHeight: 440,
        overflowX: "auto",
    },
    backdrop: {
        zIndex: theme.zIndex.drawer + 1,
        color: "#fff",
    },
}));

import React, {useState} from 'react';

import InternalSimple from '../Components/Layouts/InternalSimple';

import ModalWallet from '../Components/SaveWalletModal/SaveWalletModal'
import ConfirmationDeposity from '../Components/ConfirmationDeposity/ConfirmationDeposity'

const SaveWallet = () => {
  const [ confirmation, setConfirmation ] = useState(false);
    // When integrate api use this to setPaymentInfos 
  // const { paymentInfos, setPaymentInfos } = useContext(Context);

  return confirmation ?
    <InternalSimple>
      <ConfirmationDeposity confirmation />
    </InternalSimple>
    :
    <>
      <ModalWallet 
        onClickYes={() => setConfirmation(true)} 
        onClickNo={() => console.log("no")} 
        value="500.00 GBP" 
      />
    </>
}

export default SaveWallet;
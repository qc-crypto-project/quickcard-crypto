import React, { useState, useEffect } from 'react';

import InternalSimple from '../Components/Layouts/InternalSimple'
import ConfirmationDeposity from '../Components/ConfirmationDeposity/ConfirmationDeposity'
import Loading from '../Components/Loading/Loading'


function Deposity() {
  const [loadingPayment, setLoadingPayment] = useState(false);
  // When integrate api use this to setPaymentInfos 
  // const { paymentInfos, setPaymentInfos } = useContext(Context);

  useEffect(() => {
    // Simulate async api
    setInterval(() => {
      setLoadingPayment(true)
    }, 1000);
  }, [])

  return (
    <InternalSimple>
      {
        loadingPayment ? 
        <ConfirmationDeposity />
        :
        <Loading message="Processing your transaction" />
      }
    </InternalSimple>
  )
}


export default Deposity;
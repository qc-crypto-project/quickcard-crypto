import React from 'react';

import InternalSimple from '../Components/Layouts/InternalSimple';
import '@styles/intlTelInput.scss';

import VerifyInfos from '../Components/VerifyInfos/VerifyInfos'

function VerifyNumber() {
  return (
    <InternalSimple>
      <VerifyInfos variation="telephone"/>
    </InternalSimple>
  )
}

export default VerifyNumber;
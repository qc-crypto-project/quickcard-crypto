import React, {useState} from 'react';

import InternalSimple from '../Components/Layouts/InternalSimple'
import ConfirmationPurchase from '../Components/ConfirmationPurchase/ConfirmationPurchase'
import Loading from '../Components/Loading/Loading'
import TransactionFeedback from '../Components/TransactionFeedback/TransactionFeedback'

function PayMerchant() {
  const [purchaseComplete, setPurchaseComplete] = useState(false);
  const [loadingPurchase, setLoadingPurchase] = useState(false);
  const [transactionSuccess, setTransactionSuccess] = useState(false);

  const handlePurchaseClick = () => {
    setLoadingPurchase(true);
    setPurchaseComplete(true);
    setTransactionSuccess(true);

    // Push api implementation here
    setInterval(() => {
      setLoadingPurchase(false);
    }, 1500);
  };

  return (
    <InternalSimple>
      {
        purchaseComplete ?
          loadingPurchase ?
            <Loading message="Transferring funds to the merchant" />
            :
            <TransactionFeedback success={transactionSuccess ? true : false} />
        :
        <ConfirmationPurchase purchaseClick={handlePurchaseClick} />
      }
    </InternalSimple>
  );
}

export default PayMerchant;

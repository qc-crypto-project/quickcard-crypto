import React from 'react';

import InternalSimple from "@components/Layouts/InternalSimple"
import Footer from "@components/Layouts/Footer"
import PaymentSelectionModal from "@components/PaymentSelectionModal/PaymentSelectionModal"

function PaymentSelection() {
  return (
    <>
      <InternalSimple>
          <PaymentSelectionModal />
      </InternalSimple>
      <Footer />
    </>
   

  )
}

export default PaymentSelection;
import React, { useState } from 'react';

import { Footer } from '@components/Layouts';
import { InternalSimple } from '@components/Layouts';
import LoginForm from '@components/LoginForm/LoginForm';

export default function Login(props) {
  return (
    <>
      <InternalSimple>
        <LoginForm />
      </InternalSimple>
      <Footer />
    </>
  );
}

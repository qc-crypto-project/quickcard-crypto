import React from 'react';
import { BrowserRouter as Router, Route } from 'react-router-dom';
import { AnimatedSwitch, AnimatedRoute } from 'react-router-transition';

import { Footer } from '@components/Layouts';
import { CreateAccountForm, TwoStepVerification } from '@components';
import VerifyNumber from '@pages/VerifyNumber';
import VerifyEmail from '@pages/VerifyEmail';
import CreateAccountProvider from '../Services/Context/CreateAccountContext';

import '@styles/pages/create-account.scss';
import '@styles/intlTelInput.scss';

class CreateAccount extends React.Component {
  state = {
    step: 0,
  };

  handleSubmitCreateAccount() {
    //
  }

  render() {
    return (
      <>
        <CreateAccountProvider>
          <AnimatedSwitch
            atEnter={{ opacity: 0 }}
            atLeave={{ opacity: 0 }}
            atActive={{ opacity: 1 }}
            className="switch-wrapper"
          >
            <AnimatedRoute
              exact
              path="/create-account"
              component={CreateAccountForm}
              atEnter={{ opacity: 0 }}
              atLeave={{ opacity: 0 }}
              atActive={{ opacity: 1 }}
            />
            <AnimatedRoute
              path="/create-account/verify-phone-number/"
              component={TwoStepVerification}
              atEnter={{ opacity: 0 }}
              atLeave={{ opacity: 0 }}
              atActive={{ opacity: 1 }}
            />
            <AnimatedRoute
              path="/create-account/verify-email/"
              render={(props) => (
                <TwoStepVerification
                  {...props}
                  title="Verify your email"
                  type="Email"
                  atEnter={{ opacity: 0 }}
                  atLeave={{ opacity: 0 }}
                  atActive={{ opacity: 1 }}
                />
              )}
            />
            <AnimatedRoute
              path="/create-account/change-phone-number"
              component={VerifyNumber}
              atEnter={{ opacity: 0 }}
              atLeave={{ opacity: 0 }}
              atActive={{ opacity: 1 }}
            />
            <AnimatedRoute
              path="/create-account/change-email"
              component={VerifyEmail}
              atEnter={{ opacity: 0 }}
              atLeave={{ opacity: 0 }}
              atActive={{ opacity: 1 }}
            />
          </AnimatedSwitch>
          <Footer />
        </CreateAccountProvider>
      </>
    );
  }
}

export default CreateAccount;

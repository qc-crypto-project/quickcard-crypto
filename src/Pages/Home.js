import React from 'react';
import { Footer } from '../Components/Layouts';
import { CardRound } from '../Components/';

import '@styles/pages/home.scss';

import logo from '@assets/Images/quickcard-logo.svg';

const Home = () => {
  return (
    <>
      <div className="home">
        <div className="home__description">
          <img className="qc-logo" src={logo} alt="QuickCard" />
          <div className="home__description__texts">
            <p className="desc-text">
              QuickCard is a new payment processing system where you create a crypto wallet, buy or
              transfer coins.
            </p>
            <div className="text-feature">
              <p className="text-blue">Log in or Create a wallet</p>
              <p className="text-blue">Buy crypto</p>
              <p className="text-blue">Save or Pay</p>

              <p>
                <strong>Simple as that.</strong>
              </p>
            </div>
          </div>
        </div>
        <div className="home__card-currency">
          <CardRound
            variation="welcome"
            get={{
              currency: 'QUSD',
              value: '500.00',
            }}
            pay={{
              currency: 'GBP',
              value: '520.00',
            }}
          />
        </div>
      </div>
      <Footer />
    </>
  );
};

export default Home;

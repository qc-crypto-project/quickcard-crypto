import React from 'react';

import CardErrorComponent from "@components/CardError/CardError"
import InternalSimple from "@components/Layouts/InternalSimple"

function CardError() {
  return (
    <InternalSimple>
      <CardErrorComponent />
    </InternalSimple>
  )
}

export default CardError;
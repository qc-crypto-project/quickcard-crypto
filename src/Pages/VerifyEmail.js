import React from 'react';

import InternalSimple from '../Components/Layouts/InternalSimple';
import '@styles/intlTelInput.scss';

import VerifyInfos from '../Components/VerifyInfos/VerifyInfos'

function VerifyEmail() {
  return (
    <InternalSimple>
      <VerifyInfos variation="email"/>
    </InternalSimple>
  )
}

export default VerifyEmail;
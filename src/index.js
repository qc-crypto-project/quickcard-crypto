import React from 'react';
import ReactDOM from 'react-dom';
import './index.scss';
import App from './App';
import * as serviceWorker from './serviceWorker';
import { BrowserRouter as Router } from 'react-router-dom';
import makeServer from './serverMock';

makeServer(); //mock-api initialization

ReactDOM.render(
  <Router>
    <App></App>
  </Router>,
  document.getElementById('root')
);

serviceWorker.register();

import React from 'react';

import "./RemoveCardConfirmation.style.scss"
import { Modal } from 'greenbox_ui/src/components';

function RemoveCardConfirmation({onClose}) {
  
  return (
    <Modal 
      show
      onClick={onClose}
    >
        <div className="card-remove-confirmation">
          <h2 className="card-remove-confirmation__title">
            Card removed successfully!
          </h2>
          <p className="card-remove-confirmation__message">
            This payment method has been removed completely from your profile successfully.
          </p>
        </div>
    </Modal>
  )
}

export default RemoveCardConfirmation;
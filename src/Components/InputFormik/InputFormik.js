import React from 'react';

import './InputFormik.style.scss';
import { Label } from 'greenbox_ui/src/components';
import { Field } from 'formik';

function InputFormik({
  label,
  placeholder,
  variation,
  onChange,
  optional,
  message,
  disable,
  name,
}) {
  return (
    <div className="input-custom">
      <div className="input-custom__label">
        <Label variation="gray">{label}</Label>
        {optional ? <span className="input-custom__optional">(optional)</span> : null}
      </div>
      <Field
        name={name}
        disabled={disable}
        type={variation}
        placeholder={placeholder}
        className={'input-custom__input input-value'}
      />
      <span className="input-custom__message">{message}</span>
    </div>
  );
}

export default React.memo(InputFormik);

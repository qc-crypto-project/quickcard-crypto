import React, { useEffect, useState } from 'react';

import "./EditCardModal.style.scss"

import { CustomSelect, Input, ActionButton, Modal, Label } from "greenbox_ui/src/components"

function EditCardModal({card, onClose, onEdit}) {
  const [cardInfos, setCardInfos] = useState();
  const [addressInfos, setAddressInfos] = useState();

  useEffect(() => {
    const { cardInfos: {nameCard, expireDate} } = card
    const  { userInfos: {country, state, address1, address2, city, zip}} = card
    setCardInfos({nameCard, expireDate})
    setAddressInfos({country, state, address1, address2, city, zip})

  }, [])

  const optionsListMock = [
    { id: 1, name: "Option 1" },
    { id: 2, name: "Option 2" },
    { id: 3, name: "Option 3" },
    { id: 4, name: "Option 4" },
  ];

  const handleEditCard = () => {
    // Put api to edit card here
    onEdit();
  }


  return cardInfos ?(
    <Modal onClick={onClose} show>
      <div className="edit-card-container">
        <div className="edit-card-modal">
          <Label variation="black">Card Infos</Label> 
          <div className="edit-card-modal__card-infos">
            <div className="input-custom">
              <Input 
                label="Name on Card" 
                placeholder="Type Name on Card"
                value={cardInfos.nameCard}
              />
            </div>
            <div className="input-custom-half">
              <div className="input-custom-half__input">
                <Input 
                  label="Expiry Date" 
                  placeholder="MM/YY"
                  value={`${cardInfos.expireDate.mounth}/${cardInfos.expireDate.year}`}
                />
              </div>
            </div>
          </div>

          <Label variation="black">Billing Address</Label> 
          <div className="divisor" />
          <div className="edit-card-modal__address-infos">
            <div className="edit-card-modal__address-infos__selects">
              <div className="select-custom">
                <CustomSelect 
                  defaultText="Select Country" 
                  optionsList={optionsListMock}
                  onChange={() => console.log("Ola, mundo")}
                  label="Country"
                />
              </div>
              <div className="select-custom">
                <CustomSelect 
                  defaultText="Select" 
                  optionsList={optionsListMock}
                  onChange={() => console.log("Ola, mundo")}
                  label="State/Province/Region"
                />
              </div>
            </div>
            <div className="edit-card-modal__address-infos__address">
              <div className="input-custom">
                <Input 
                  label="Address Line 1"
                  placeholder="Enter Address"
                  onChange={() => console.log("Input address change")}
                  value={addressInfos.address1}
                />
              </div>
              <div className="input-custom">
                <Input 
                  label="Address Line 2"
                  placeholder="Enter Address"
                  onChange={() => console.log("Input address change")}
                  value={addressInfos.address2}
                />
              </div>
            </div>
            <div className="edit-card-modal__address-infos__city-zip">
              <div className="input-custom">
                <Input 
                  label="City"
                  placeholder="Enter City"
                  onChange={() => console.log("Input city-zip change")}
                  value={addressInfos.city}
                />
              </div>
              <div className="input-custom">
                <Input 
                  label="Zip/Postal Code"
                  placeholder="000"
                  onChange={() => console.log("Input city-zip change")}
                  value={addressInfos.zip}
                />
              </div>
            </div>
          </div>
          <div className="edit-card-modal__buttons">
            <ActionButton
              onClick={() => onClose()}
              color="red"
            >
              Cancel
            </ActionButton>
            <ActionButton
              onClick={handleEditCard}
              color="green"
            >
              Save
            </ActionButton>
          </div>
        </div>
      </div>
    </Modal>
  )
  :
  null
}

export default EditCardModal;
import React from 'react';
import { InternalSimple } from '@components/Layouts';

import './CodeWarning.style.scss';

function CodeWarning() {
  return (
    <InternalSimple>
      <div className="code-warning">
        <h1>
          It looks like you are having trouble with receiving the code. <br /> Please try again
          later.
        </h1>
        <div className="separator-2lines">
          <p>Or</p>
        </div>

        <div className="support-box">
          <h2>Contact support</h2>
          <div className="support-box__row">
            <span className="support-box__row--icon">
              <i class="fa fa-envelope" aria-hidden="true"></i>
            </span>
            <p>support@quickcard.me</p>
          </div>

          <div className="support-box__row">
            <span className="support-box__row--icon">
              <i class="fa fa-phone" aria-hidden="true"></i>
            </span>
            <p>619-333-4227</p>
          </div>

          <div className="support-box__row">
            <span className="support-box__row--icon">
              <i class="fa fa-clock" aria-hidden="true"></i>
            </span>
            <p>9am - 5pm PST, Monday - Friday</p>
          </div>
        </div>
      </div>
    </InternalSimple>
  );
}

export default CodeWarning;

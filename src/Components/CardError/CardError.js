import React from 'react';

import './CardError.style.scss';

import { Button } from 'greenbox_ui/src/components';

import cardErrorImg from '@assets/Images/card-error.svg';

function CardError() {
  return (
    <div className="card-error">
      <img alt="Card error" src={cardErrorImg} />
      <span className="card-error__title">Something went wrong while processing your card.</span>
      <span className="card-error__message">Card Declined. Error code 3003.</span>
      <Button variation="primary">Try Again</Button>
    </div>
  );
}

export default CardError;

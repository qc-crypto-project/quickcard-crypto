import React from 'react';

import loading from '@assets/Images/loading.svg';

import './LoadingStep.style.scss';

const LoadingStep = ({ msg }) => {
  return (
    <div className="loading-step">
      <img src={loading} alt="Loading" className="loading-step__img" />
      <p className="loading-step__msg">{msg}</p>
    </div>
  );
};

export default LoadingStep;

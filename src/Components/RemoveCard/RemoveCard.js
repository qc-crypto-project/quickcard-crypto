import React, {useState} from 'react';

import "./RemoveCard.style.scss"

import {Button, Modal} from "greenbox_ui/src/components"

function RemoveCard({cardId, onRemove, onClickNo, onClose}) {
  const [cardRemoved, setCardRemoved] = useState()

  const handleRemoveCard = () => {
    // Put api to remove card here
    const removedWithSuccess = true

    if(removedWithSuccess){
      setCardRemoved(true)
      onRemove(true)
    } 

    else{
      onRemove(false)
    }
  }

  return (
    <Modal show onClick={onClose}>
        <div className="card-remove">
          <h2 className="card-remove__title">
            Are you sure you want to remove this card?
          </h2>
          <p className="card-remove__message">
            If you do not want this payment method to be displayed in your list of payment options, click "Confirm remove". (Disabling this payment method will not cancel any of your open orders that use this method.)
          </p>
          <div className="card-remove__buttons">
            <div className="card-remove__buttons--cancel">
              <Button variation="secondary" onClick={onClickNo}>
                Cancel
              </Button>
            </div>
            <div className="card-remove__buttons--confirm" onClick={handleRemoveCard}>
              <Button variation="primary">
                Confirm Remove
              </Button>
            </div>
          </div>
        </div>
    </Modal>
  )
}

export default RemoveCard;
import React from 'react';

import './TransactionFeedback.style.scss';
import successTransaction from '@assets/Images/hand.svg';
import errorTransaction from '@assets/Images/error-transaction.svg';
import { Button } from 'greenbox_ui/src/components';

function TransactionFeedback({ success }) {
  const handleRedirectToPayMerchant = () => {
    window.location.href = '/pay-merchant';
  };

  return (
    <div className="transaction">
      <img
        className="transaction__image"
        src={success ? successTransaction : errorTransaction}
        alt="TransactionFeedback"
      />
      <h3 className={`transaction__title ${success ? 'success-title' : 'error-title'}`}>
        {success ? 'Transaction Successful!' : 'Something went wrong with your transfer'}
      </h3>
      {success ? <div className="divisor" /> : null}
      <div className="transaction__content content-line">
        {success ? (
          <>
            <span className="transaction__content__message">
              <span className="transaction__content__message__value">500.00 QUSD</span>
              have been successfully paid to [merchantwebsite.com].
            </span>
            <div className="transaction-id">
              <span className="transaction-id__label">QC Transaction ID</span>
              <span className="transaction-id__value">7676347589123...7676347589123</span>
            </div>
          </>
        ) : (
          <span className="transaction__content__error">
            This text paragraph is to save space for a more elaborate error message, with ID codes
            or numbers to help the user eventually.
          </span>
        )}
      </div>
      {success ? (
        <>
          <div className="divisor--half" />
          <div className="transaction__footer">
            <span>
              Please wait, we are redirecting you back to [merchant.url] <br /> If you don't get
              redirected in [10] seconds, please
              <a href="http://localhost:3000" className="redirect">
                {' '}
                click here.{' '}
              </a>
              {/* TODO : In prodution change this url  */}
            </span>
          </div>
        </>
      ) : (
        <Button onClick={handleRedirectToPayMerchant} variation="primary">
          Try Again
        </Button>
      )}
    </div>
  );
}

export default TransactionFeedback;

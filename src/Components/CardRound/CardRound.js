import React, { useState } from 'react';
import PropTypes from 'prop-types';
import { Link } from 'react-router-dom';

import Purchase from '../Purchase/Purchase';
import { BigButton, CurrencyField, CurrencySelector } from 'greenbox_ui/src/components';

import './CardRound.style.scss';

function CardRound({ variation, pay, get }) {
  const [currency, setCurrency] = useState();

  return (
    <div className="currency-box">
      <div className="currency-box__header">
        <Purchase balanceCurrency={pay.currency} balanceValue={pay.value} />
      </div>
      <div className="currency-box__divisor" />
      <div className="currency-box__body">
        <div className="currency-box__body__selector">
          <CurrencySelector 
            onChange={(currencySelected) => setCurrency(currencySelected)} 
          />
        </div>
        <CurrencyField
          getBalance={get.value}
          getCurrency={get.currency}
          payBalance={pay.value}
          payCurrency={pay.currency}
        />
      </div>
      <div className="currency-box__footer">
        {variation === 'welcome' ? (
          <div className="currency-box__footer__buttons">
            <Link to="/create-account">
              <BigButton color="green">First Time User</BigButton>
            </Link>
            <Link to="/">
              <BigButton color="yellow">Existing User</BigButton>
            </Link>
          </div>
        ) : (
          <div className="currency-box__footer__card">
            <div className="currency-box__footer__card__divisor" />
            <div className="currency-box__footer__grand">
              <span className="currency-box__footer__grand__total"> Grand Total </span>
              <div>
                <span className="currency-box__footer__grand__value">{pay.value}</span>
                <span className="currency-box__footer__grand__currency">{pay.currency}</span>
              </div>
            </div>
          </div>
        )}
      </div>
    </div>
  );
}

CardRound.propTypes = {
  variation: PropTypes.string,
  get: PropTypes.shape({
    value: PropTypes.string.isRequired,
    currency: PropTypes.string.isRequired,
  }),
  pay: PropTypes.shape({
    value: PropTypes.string.isRequired,
    currency: PropTypes.string.isRequired,
  }),
};

export default CardRound;

import React, { useState } from 'react';
import ReactIntlTelInput from 'react-intl-tel-input-v2';
import { Formik, Field } from 'formik';
import * as Yup from 'yup';
import { useHistory } from 'react-router-dom';
import axios from 'axios';

import { StorageService } from '@services/Storage/storageService';
import { InternalSimple } from '@components/Layouts';
import { Button, LinkButton, Checkbox } from 'greenbox_ui/src/components';
import { useCreateAccount } from '../../Services/Context/CreateAccountContext';

import './CreateAccountForm.style.scss';
import '@styles/intlTelInput.scss';

const terms = {
  termsCheck: false,
  policyCheck: false,
};

const SignupSchema = Yup.object().shape({
  firstName: Yup.string().required('Please enter your First Name'),
  lastName: Yup.string().required('Please enter your Last Name'),
  email: Yup.string().email('Invalid email').required('Please enter your Email'),
});

const intlTelOpts = {
  initialCountry: 'us',
};

export default function CreateAccountForm() {
  const history = useHistory();
  const { updateUserData, userData } = useCreateAccount();
  const [state, setState] = useState({
    phoneError: {
      isError: false,
      msg: '',
    },
    nextDisable: true,
    value: { iso2: 'us', dialCode: '1', phone: '' },
  });

  const updateState = (value) => {
    setState((prev) => ({ ...prev, ...value }));
  };

  const checkTerms = () => {
    if (terms.termsCheck === true && terms.policyCheck === true) {
      updateState({ nextDisable: false });
    } else {
      updateState({ nextDisable: true });
    }
  };

  const handleChange = (e) => {
    terms[e.target.id + 'Check'] = e.target.checked;
    checkTerms();
  };

  return (
    <InternalSimple>
      <div className="create-account">
        <h1 className="internal-title text-blue">
          Let's start by <br /> creating your account
        </h1>
        <Formik
          initialValues={{
            firstName: '',
            lastName: '',
            email: '',
          }}
          validationSchema={SignupSchema}
          onSubmit={async (values) => {
            const phoneNumber = state.value.phone;
            if (phoneNumber.length >= 6) {
              updateUserData({
                firstName: values.firstName,
                lastName: values.lastName,
                phoneData: state.value,
                phoneNumber: state.value.dialCode + state.value.phone,
                email: values.email,
              });
              try {
                const response = await axios.post('/api/create-user', {
                  firstName: values.firstName,
                  lastName: values.lastName,
                  phoneData: state.value,
                  phoneNumber: state.value.dialCode + state.value.phone,
                  email: values.email,
                });
                if (response.data.success) {
                  history.push('/create-account/verify-phone-number');
                }
              } catch {
                updateState({
                  phoneError: { isError: true, msg: 'Phone number already registered' },
                });
              }
              // const formData = {
              //   ...values,
              //   phone: this.state.value,
              // };
            } else {
              updateState({
                phoneError: { isError: true, msg: 'Please enter your phone number' },
              });
            }
          }}
        >
          {({ errors, touched, handleSubmit }) => (
            <form className="forms" onSubmit={handleSubmit}>
              <div className="form create-account__form">
                <div className="form-row">
                  <label className="form__label">First Name</label>
                  <div
                    className={`form__input ${
                      touched.firstName && errors.firstName ? 'form-error' : ''
                    }`}
                  >
                    <Field name="firstName" type="text" className="input-text" />
                    {errors.firstName && touched.firstName ? (
                      <span className="error-form-label">{errors.firstName}</span>
                    ) : null}
                  </div>
                </div>

                <div className="form-row">
                  <label className="form__label">Phone Number</label>
                  <ReactIntlTelInput
                    className={`phone-input ${state.phoneError.isError ? 'phone-error' : ''}`}
                    intlTelOpts={intlTelOpts}
                    value={state.value}
                    onChange={(value) => updateState({ value: value })}
                  />
                  {state.phoneError.isError ? (
                    <span className="error-form-label">{state.phoneError.msg}</span>
                  ) : null}
                </div>

                <div className="form-row">
                  <label className="form__label">Last Name</label>
                  <div
                    className={`form__input ${
                      touched.firstName && errors.firstName ? 'form-error' : ''
                    }`}
                  >
                    <Field name="lastName" type="text" className="input-text" />
                  </div>
                  {errors.lastName && touched.lastName ? (
                    <span className="error-form-label">{errors.lastName}</span>
                  ) : null}
                </div>

                <div className="form-row">
                  <label className="form__label">Email</label>
                  <div
                    className={`form__input ${touched.email && errors.email ? 'form-error' : ''}`}
                  >
                    <Field name="email" type="email" className="input-text" />
                  </div>
                  {errors.email && touched.email ? (
                    <span className="error-form-label">{errors.email}</span>
                  ) : null}
                </div>

                <div className="agree-row">
                  <Checkbox onChange={handleChange} id="terms" />
                  <p>
                    I Agree with the <span className="open-modal">Terms of Service</span>
                  </p>
                </div>
                <div className="agree-row">
                  <Checkbox onChange={handleChange} id="policy" />
                  <p>
                    I Agree with the <span className="open-modal">Privacy Policy</span>
                  </p>
                </div>
                <Button type="submit" disable={state.nextDisable}>
                  Next
                </Button>

                <div className="login-link">
                  <p>Already have an account?</p>
                  <LinkButton onClick={() => history.push('/login')}>Please Login</LinkButton>
                </div>
              </div>
            </form>
          )}
        </Formik>
      </div>
    </InternalSimple>
  );
}

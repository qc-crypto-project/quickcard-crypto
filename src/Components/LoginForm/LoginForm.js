import React, { useState } from 'react';
import ReactIntlTelInput from 'react-intl-tel-input-v2';
import { Formik, Field, Form } from 'formik';
import * as Yup from 'yup';
import { useHistory } from 'react-router-dom';
import axios from 'axios';

import { Button, LinkButton } from 'greenbox_ui/src/components';

import './LoginForm.style.scss';
import '@styles/intlTelInput.scss';

const loginSchema = Yup.object().shape({
  password: Yup.string().required('Please enter your password'),
});

export default function LoginForm() {
  const history = useHistory();
  const [tel, setTel] = useState({ value: { iso2: 'us', dialCode: '1', phone: '' }, error: null });
  const [isLoading, setIsLoading] = useState(false);
  const [loginError, setLoginError] = useState(false);

  //phone validation
  const onSubmit = (values) => {
    if (!tel.value.phone) {
      setTel((prev) => ({ ...prev, error: 'Please enter a phone number' }));
      return;
    }
    submitData({
      ...values,
      tel: `${tel.value.dialCode}${tel.value.phone}`.replace(/(,|[^\d.-]+)+/g, ''),
    });
  };

  //api call
  const submitData = async (data) => {
    setIsLoading(true);
    setLoginError(false);

    //api post request
    try {
      const response = await axios.post('api/authenticate', {
        login: data.tel,
        password: data.password,
      });
      setIsLoading(false);
      console.log('AUTHORIZED', response.data);
      //to-do: add context-based authorization
      localStorage.setItem('jwt', response.data.token);
      alert('Successfully logged in!');
    } catch (e) {
      setLoginError(true);
      setIsLoading(false);
      console.log('UNAUTHORIZED', e.response.status);
    }
  };

  //intl tel input
  const onChangeTel = (value) => {
    if (!isLoading) {
      setTel({ value: value, error: null });
      setLoginError(false);
    }
  };

  return (
    <div className="login">
      <h1 className="internal-title text-blue">
        Welcome back! <br /> Please, log in.
      </h1>
      <Formik
        initialValues={{
          password: '',
        }}
        onSubmit={onSubmit}
        validationSchema={loginSchema}
        validate={false}
      >
        {({ errors, touched }) => (
          <Form>
            <div className="form login__form">
              <div className="form-row">
                <label className="form__label">Phone Number</label>
                <ReactIntlTelInput
                  className={`phone-input ${tel.error || loginError ? 'phone-error' : ''} ${
                    isLoading ? 'phone-disabled' : ''
                  }`}
                  intlTelOpts={{ initialCountry: 'us' }}
                  value={tel.value}
                  onChange={onChangeTel}
                  name="tel"
                  inputProps={{ disabled: isLoading }}
                />
                {tel.error ? <span className="error-form-label">{tel.error}</span> : null}
                {loginError ? (
                  <span className="error-form-label">Invalid number or password</span>
                ) : null}
              </div>
              <div className="form-row">
                <label className="form__label">Password</label>
                <div
                  className={`form__input ${
                    (touched.password && errors.password) || loginError ? 'form-error' : ''
                  }`}
                >
                  <Field
                    name="password"
                    type="password"
                    className="input-text"
                    disabled={isLoading}
                  />
                </div>
                {errors.password && touched.password ? (
                  <span className="error-form-label">{errors.password}</span>
                ) : null}
                {loginError ? (
                  <span className="error-form-label">Invalid number or password</span>
                ) : null}
              </div>

              <div className="form-row">
                <Button disable={isLoading} type="submit">
                  Next
                </Button>
              </div>
              <div className="forgot-password-btn">
                <LinkButton onClick={() => history.push('/')}>Forgot Password?</LinkButton>
              </div>

              <div className="login-link">
                <p>New to QuickCard?</p>
                <LinkButton disable={isLoading} onClick={() => history.push('/create-account')}>
                  Create your QuickCard account
                </LinkButton>
              </div>
            </div>
          </Form>
        )}
      </Formik>
    </div>
  );
}

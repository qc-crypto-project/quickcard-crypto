import React from 'react';

import "./PaymentSelectionModal.style.scss"

import Purchase from '../Purchase/Purchase'
import {Button} from "greenbox_ui/src/components/styleguide/Buttons/Buttons"

import MyBalanceImage from '../../Assets/Images/deposity.svg'
import CardImage from '../../Assets/Images/cards.svg'

function PaymentSelectionModal() {
 
  const payWithMyBalance = () => {
    window.location.href = "/pay-merchant"
  }

  const addFundsWithCard = () => {
    window.location.href = "/cards"
  }

  return (
    <div className="payment-selection-container">
      <div className="payment-selection-modal">
        <header className="payment-selection-modal__header">
          <div className="payment-selection-modal__header__welcome">
            <p>
              Hello, 
              <span className="payment-username">
                John Smith
              </span>
            </p>
            <span className="account-id">
              Account C-9876542365
            </span>
          </div>
        </header>
        <div className="payment-selection-modal__purchase">
          <Purchase balanceCurrency="QUSD" balanceValue="400.00" />
        </div>
        <div className="payment-selection-modal__body">
          <span className="payment-selection-modal__body__message">
            How would you like to pay?
          </span>
          <div className="methods">
            <div className="methods__my-balance">
              <img 
                src={MyBalanceImage}
                alt="My balace"
              />
              <Button variation="primary" onClick={payWithMyBalance}>
                Pay with my balance
              </Button>
              <div className="methods__my-balance__footer">
                <span>
                  Available Balance
                </span>
                <span className="methods__my-balance__footer__value">
                  420.00 QUSD
                </span>
              </div>
            </div>
            <div className="methods__divisor">
              <div className="divisor-half" />
              <span> OR </span>
              <div className="divisor-half" />
            </div>
            <div className="methods__card">
              <img 
                src={CardImage}
                alt="Card"
              />
              <Button variation="primary" onClick={addFundsWithCard}>
                Add funds with a Card
              </Button>
            </div>
          </div>
        </div>
      </div>
    </div>
  )
}

export default PaymentSelectionModal;
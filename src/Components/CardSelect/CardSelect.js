import React, { useEffect, useState } from 'react';
import PropTypes from 'prop-types'
import CreditCardType from "credit-card-type"

import "./CardSelect.style.scss"

import Edit from '@assets/Images/edit.svg'
import Trash from '@assets/Images/trash.svg'
import Info from "@assets/Images/info.svg"

import alipay from 'payment-icons/min/flat/alipay.svg';
import amex from 'payment-icons/min/flat/amex.svg';
import diners from 'payment-icons/min/flat/diners.svg';
import discover from 'payment-icons/min/flat/discover.svg';
import elo from 'payment-icons/min/flat/elo.svg';
import hipercard from 'payment-icons/min/flat/hipercard.svg';
import jcb from 'payment-icons/min/flat/jcb.svg';
import maestro from 'payment-icons/min/flat/maestro.svg';
import mastercard from 'payment-icons/min/flat/mastercard.svg';
import paypal from 'payment-icons/min/flat/paypal.svg';
import verve from 'payment-icons/min/flat/verve.svg';
import visa from 'payment-icons/min/flat/visa.svg';

import {Radio, Input, Button, ActionButton} from "greenbox_ui/src/components"

const CardSelect = ({cardInfos, onRemove, onEdit}) => {
  const [dropdown, setDropdown] = useState();
  const [cvc, setCvc] = useState(0);
  const [expired, setExpired] = useState();
  const [card, setCard] = useState({})
  const [errors, setErrors] = useState();
  const {cardInfos: {cardNumber, expireDate}, userInfos} = cardInfos

  useEffect(() => {
    const date = new Date();
    const mounth = date.getMonth() === 12 ? 1 : date.getMonth() + 1
    const year = date.getFullYear()

    const verifyExpired = verifyExpiration(mounth, year)    
    setExpired(verifyExpired)

    const [userCard] = CreditCardType(cardNumber)
    const { niceType } = userCard
    setCard({
      niceType,
      expireDate: `${mounth}/${year}`
    })

  }, [])

  const verifyExpiration = (mounth, year) => {
    const verifyYear = year > expireDate.year
    const verifyMounth = mounth > expireDate.mounth

    if(verifyYear) return true
    if(year === expireDate){
      return verifyMounth ? true : false
    }

    return false
  }

  const handleValidateCVC = () => {
    // Put api to validate cvc here
    const validCVC = false;
    if(validCVC){
      window.location.href = "/deposity"
    }
    else{
      setErrors({cvc: "Invalid Code"})
    }
  }

  return card.niceType ? (
    <div 
      className={`
        card-select 
        ${dropdown && !expired ? "card-select--selected" : null}
        ${expired ? "card-select--expired" : null}
        `}
    > 
      <div className="card-select--close" onClick={() => setDropdown(!dropdown)}> 
        <div className="card-select__card-infos">
          {expired ? null : <Radio />}
          
          <ImageByCardType cardType={card.niceType} />

          <span
            className="card-select__card-infos__card-number">
              **** {cardNumber}
          </span>
        </div>
        <div className="card-select__operations">
          {
            expired ? 
              <div className="card-select__operations__expired">
                <ActionButton color="red">
                  Expired {card.expireDate}
                </ActionButton>
              </div>
              :
              <img
                src={Edit}
                className="card-select__operations--edit"
                onClick={onEdit}
                alt="Edit"
              />
          }
          
          <img
            src={Trash}
            className={`card-select__operations--delete ${expired ? "card-select__operations--expired" : null }`}
            onClick={onRemove}
            alt="Delete"
          />
        </div>
      </div>
      {
        dropdown && !expired ?
        <div className="card-select__adicional-infos">
          <div className="card-select__adicional-infos__user-infos">
            <span>{userInfos.name}</span>
            <p>
              {userInfos.address1}, {userInfos.city}, {userInfos.country} 
            </p>
            <div className="infos-cvc">
              <span>CVC</span>
              <img
                className="infos-cvc__warning"
                src={Info}
                alt="Warning"
              />
              <div className={`${errors && errors.cvc ? "error-cvc" : null} infos-cvc__input`}>
                <Input
                  onChange={(e) => setCvc(e.target.value)}
                />
              </div>
            </div>
          </div>
          <div className="card-select__adicional-infos__confirmation">
            <span>
              Exp. Date: {card.expireDate}
            </span>
            <Button 
              variation="primary"
              onClick={handleValidateCVC}
              disable={cvc.toString().length < 3 ? true : false}  
            >
              Confirm
            </Button>
          </div>
        </div>
        :
        null
      }
    </div>
  )
  :
  null
}


const ImageByCardType = ({cardType}) => {
  const cardTypeLowerCase = cardType.toLowerCase().trim()
    
  switch (cardTypeLowerCase) {
    case "alipay":
      return (
        <img 
          src={alipay}
          className="card-type-custom"
          alt="alipay flag"
        />
      )

    case "amex":
      return (
        <img 
          src={amex}
          className="card-type-custom"
          alt="amex flag"
        />
      )

    case "diners":
      return (
        <img 
          src={diners}
          className="card-type-custom"
          alt="diners flag"
        />
      )

    case "discover":
      return (
        <img 
          src={discover}
          className="card-type-custom"
          alt="discover flag"
        />
      )

    case "elo":
      return (
        <img 
          src={elo}
          className="card-type-custom"
          alt="elo flag"
        />
      )

    case "hipercard":
      return (
        <img 
          src={hipercard}
          className="card-type-custom"
          alt="hipercard flag"
        />
      )

    case "jcb":
      return (
        <img 
          src={jcb}
          className="card-type-custom"
          alt="jcb flag"
        />
      )

    case "maestro":
 
      return (
        <img 
          src={maestro}
          className="card-type-custom"
          alt="maestro flag"
        />
      )

    case "mastercard":
      return (
        <img 
          src={mastercard}
          className="card-type-custom"
          alt="mastercard flag"
        />
      )
    case "paypal":
      return (
        <img 
          src={paypal}
          className="card-type-custom"
          alt="paypal flag"
        />
      )
    case "verve":
      return (
        <img 
          src={verve}
          className="card-type-custom"
          alt="verve flag"
        />
      )
    case "visa":
      return (
        <img 
          src={visa}
          className="card-type-custom"
          alt="visa flag"
        />
      )
                                                                                                                                        
    default:
      break;
  }

}


CardSelect.propTypes = {
  cardInfos: PropTypes.shape({
    cardNumber: PropTypes.string,
    expireDate: PropTypes.shape({
      mounth: PropTypes.number,
      year: PropTypes.number
    }),
    userInfos: PropTypes.shape({
      name: PropTypes.string,
      address: PropTypes.string
    })
  })
};

export default CardSelect;
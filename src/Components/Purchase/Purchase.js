import React from 'react';

import './Purchase.style.scss'


const Purchase = ({balanceValue, balanceCurrency}) => (
  <div className="purchase">
    <p className="purchase__title">Website Purchase Amount</p>
    <div className="purchase__balance">
      <h4 className="purchase__balance__value">{balanceValue}</h4>
      <span className="purchase__balance__currency">{balanceCurrency}</span>
    </div>
  </div>
)

export default Purchase;
### Purchase component example:

In props you can pass balanceValue and balanceCurrency
###### Examples:
```js
<Purchase balanceValue={500.00} balanceCurrency="GBP" />
```

To render an example as highlighted source code add a `static` modifier:
```jsx static
import Purchase from "../components/styleguide/Purchase/Purchase";
```

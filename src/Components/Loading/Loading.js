import React from 'react';

import "./Loading.style.scss"

import ringYellow from '../../Assets/Images/ring_yellow.svg'
import ringWhite from '../../Assets/Images/ring_white.svg'

function Loading({message}) {
  return (
    <div className="loading">
      <img
        alt=""
        className="loading__ring-yellow"
        src={ringYellow}
      />
      <img
        alt=""
        className="loading__ring-white"
        src={ringWhite}
      />
      <h3 className="loading__message">{message}</h3>
    </div>
  );
}

export default Loading;
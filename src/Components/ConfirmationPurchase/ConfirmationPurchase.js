import React from 'react';
import './ConfirmationPurchase.style.scss';

import { Button } from 'greenbox_ui/src/components';
import PaymentInfo from '../PaymentInfo/PaymentInfo';

import cartImage from '@assets/Images/cartImage.svg';

function CompletePurchase({ purchaseClick }) {
  const handleSaveWallet = () => {
    window.location.href = '/save-wallet';
  };

  return (
    <div className="confirmation-purchase">
      <div className="confirmation-purchase__header">
        <img
          alt="Confirmation purchase card"
          className="confirmation-purchase__header__image"
          src={cartImage}
        />
        <h2 className="confirmation-purchase__header__title">
          Would you like to complete your purchase with [merchant.url]?
        </h2>
        <span className="confirmation-purchase__header__message"></span>
      </div>
      <div className="divisor" />
      <div className="confirmation-purchase__confirmation">
        <div className="confirmation-purchase__confirmation__info">
          <PaymentInfo info="Total to be paid:" value="500.00 QUSD" />
        </div>
      </div>
      <>
        <div className="confirmation-purchase__buttons">
          <div className="confirmation-purchase__buttons__button">
            <Button variation="primary" onClick={purchaseClick}>
              Complete Purchase
            </Button>
          </div>

          <div className="divisor-or">
            <div className="divisor-or__line" />
            <span className="divisor-or__message">OR</span>
            <div className="divisor-or__line" />
          </div>

          <div className="confirmation-purchase__buttons__button">
            <Button variation="secondary" onClick={handleSaveWallet}>
              Save to my wallet
            </Button>
          </div>
        </div>
      </>
    </div>
  );
}

export default CompletePurchase;

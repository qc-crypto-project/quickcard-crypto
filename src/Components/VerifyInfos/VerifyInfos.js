import React, { useState } from 'react';
import { Formik, Field } from 'formik';
import * as Yup from 'yup';
import { useHistory } from 'react-router-dom';

import { Button } from 'greenbox_ui/src/components';
import ReactIntlTelInput from 'react-intl-tel-input-v2';
import { useCreateAccount } from '../../Services/Context/CreateAccountContext';

import '@styles/intlTelInput.scss';

import './VerifyInfos.style.scss';

const intlTelOpts = {
  initialCountry: 'us',
};

const SignupSchema = Yup.object().shape({
  email: Yup.string().email('Invalid email').required('Please enter your Email'),
});

export default function VerifyInfos(props) {
  const history = useHistory();
  const { updateUserData } = useCreateAccount();
  const [state, setState] = useState({
    phoneError: {
      isError: false,
      msg: '',
    },
    value: { iso2: 'us', dialCode: '1', phone: '' },
  });

  const updateState = (value) => setState((prev) => ({ ...prev, ...value }));

  const handleNextPhone = () => {
    const phoneNumber = state.value.phone;
    const userPhone = state.value;

    if (phoneNumber.length >= 6) {
      updateUserData({ phoneData: userPhone, phoneNumber: state.value.dialCode + phoneNumber });
      history.push('/create-account/verify-phone-number');
    } else {
      updateState({
        phoneError: { isError: true, msg: 'Please enter your phone number' },
      });
    }
  };

  return props.variation === 'email' ? (
    <div className="verify">
      <h4 className="verify__title">Verify your email</h4>
      <span className="verify__message">Please provide an email for verification</span>
      <Formik
        initialValues={{
          email: '',
        }}
        validationSchema={SignupSchema}
        onSubmit={(values) => {
          updateUserData({ email: values.email });
          history.push('/create-account/verify-email');
        }}
      >
        {({ errors, touched, handleSubmit }) => (
          <form className="change-user-email-form" onSubmit={handleSubmit}>
            <div className="form-row">
              <label className="form__label">Email</label>
              <div className={`form__input ${touched.email && errors.email ? 'form-error' : ''}`}>
                <Field
                  name="email"
                  type="email"
                  placeholder="email@email.com"
                  className="input-text"
                />
              </div>
              {errors.email && touched.email ? (
                <span className="error-form-label">{errors.email}</span>
              ) : null}
            </div>
            <div className="center-row">
              <Button type="submit" variation="primary">
                Next
              </Button>
            </div>
          </form>
        )}
      </Formik>
    </div>
  ) : (
    <div className="verify">
      <h4 className="verify__title">Verify your phone number</h4>
      <span className="verify__message">Please provide a phone number for validation</span>
      <div className="verify__phone">
        <label>Phone Number</label>
        <ReactIntlTelInput
          className={`phone-input ${state.phoneError.isError ? 'phone-error' : ''}`}
          intlTelOpts={intlTelOpts}
          value={state.value}
          onChange={(value) => updateState({ value: value })}
        />
        {state.phoneError.isError ? (
          <span className="error-form-label">{state.phoneError.msg}</span>
        ) : null}
      </div>
      <Button onClick={handleNextPhone} variation="primary">
        Next
      </Button>
    </div>
  );
}

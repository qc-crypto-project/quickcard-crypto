import React from 'react';

import './HelpModal.style.scss';

import email from '@assets/Images/email.svg';
import cel from '@assets/Images/celphone.svg';
import clock from '@assets/Images/clock.svg';
import close from '@assets/Images/close.svg';

function HelpModal({ onClose }) {
  return (
    <div className="help-modal-background">
      <div className="help-modal">
        <div className="help-modal__header">
          <span>Need Help?</span>
          <img
            alt="Close modal"
            className="help-modal__header__close"
            src={close}
            onClick={onClose}
          />
          <div className="divisor" />
        </div>
        <span className="help-modal__message">Please contact us.</span>
        <div className="help-modal__body">
          <div className="help-modal__body__item">
            <img alt="Email" src={email} />
            <div className="content">
              <span className="content__title"> Email </span>
              <span className="content__value"> support@quickcard.me </span>
            </div>
          </div>
          <div className="help-modal__body__item">
            <img alt="Celphone" src={cel} />
            <div className="content">
              <span className="content__title"> Customer Support </span>
              <span className="content__value"> 619-333-4227 </span>
            </div>
          </div>
          <div className="help-modal__body__item">
            <img alt="Clock" src={clock} />
            <div className="content">
              <span className="content__title"> Available from </span>
              <span className="content__value"> 9am - 5pm PST, Monday - Friday </span>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}

export default HelpModal;

import Base from './Base';
import Footer from './Footer';
import Header from './Header';
import InternalSimple from './InternalSimple';

export { Base, Footer, InternalSimple, Header };

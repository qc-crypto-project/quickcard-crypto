import React, { useState, useCallback } from 'react';
import { useHistory } from 'react-router-dom';

import HelpModal from '../HelpModal/HelpModal';

import logo from '@assets/Images/quickcard-logo.svg';
import helpImg from '@assets/Images/help.svg';

const Header = () => {
  const [showModal, setShowModal] = useState(false);
  const history = useHistory();
  const handleOnClick = useCallback(() => history.push('/'), [history]);

  return (
    <>
      {showModal ? <HelpModal onClose={() => setShowModal(false)} /> : null}
      <header className="custom-header">
        <img src={logo} alt="Logo" onClick={handleOnClick} />
        <img alt="Close" className="help" src={helpImg} onClick={() => setShowModal(true)} />
      </header>
    </>
  );
};

export default Header;

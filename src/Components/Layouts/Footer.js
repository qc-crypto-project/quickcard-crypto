import React from 'react';

import powered from '@assets/Images/poweredby.svg';
import circle from '@assets/Images/circle_effect.png';

const Footer = (props) => {
  return (
    <div className={`main-footer ${!props.fixed ? 'footer-relative' : 'footer-fixed'}`}>
      <div className="powered-by">
        <img className="powered-by__img" src={powered} alt="powered by GreenBox" />
      </div>
      <div className="main-footer--blue-bar"></div>
      <div className="main-footer__circle">
        <img alt="Circle" src={circle} className="circle-img" />
      </div>
    </div>
  );
};

export default Footer;

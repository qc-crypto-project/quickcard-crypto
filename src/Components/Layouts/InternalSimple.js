import React from 'react';
import PropTypes from 'prop-types';

import { Header } from '@components/Layouts';

import logo from '@assets/Images/quickcard-logo.svg';

const InternalSimple = ({ children, padding, styleName }) => {
  return (
    <div className={`internal-simple ${styleName ? styleName : ''}`}>
      <Header />
      <section className={`internal-simple__body ${padding ? 'internal-padding' : ''}`}>
        {children}
      </section>
    </div>
  );
};

InternalSimple.propTypes = {
  styleName: PropTypes.string,
  padding: PropTypes.bool,
};

InternalSimple.defaultProps = {
  padding: true,
};

export default InternalSimple;

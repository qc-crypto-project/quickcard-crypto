import React, { useState, useRef, useEffect } from 'react';
import PropTypes from 'prop-types';
import { useHistory } from 'react-router-dom';
import axios from 'axios';

import { InternalSimple } from '@components/Layouts';
import { LinkButton } from 'greenbox_ui/src/components';
import loadingGray from '@assets/Images/loading_gray.svg';
import { LoadingStep } from '@components';
import { useCreateAccount } from '../../Services/Context/CreateAccountContext';
import { Validator } from '../../Services/Validation';

import './TwoStepVerification.style.scss';
import { update } from 'lodash';
import { findAllByDisplayValue } from '@testing-library/react';

const fields = [
  {
    name: 'code0',
  },
  {
    name: 'code1',
  },
  {
    name: 'code2',
  },
  {
    name: 'code3',
  },
  {
    name: 'code4',
  },
  {
    name: 'code5',
  },
];

export default function TwoStepVerification(props) {
  const history = useHistory();
  const { userData } = useCreateAccount();
  const lastDigits = userData.phoneNumber.substr(userData.phoneNumber.length - 4);
  const codeInputRef = useRef([]);
  const [state, setState] = useState({
    code0: '',
    code1: '',
    code2: '',
    code3: '',
    code4: '',
    code5: '',
    userPhone: userData.phoneNumber,
    userEmail: userData.email,
    lastNumber: lastDigits,
    isChecking: false,
    isLoading: false,
    isError: false,
    isValid: false,
    statusMsg: props.statusMsg,
    requestSent: false,
  });

  const updateState = (value) => {
    setState((prev) => ({ ...prev, ...value }));
  };

  //focus first input on load
  useEffect(() => {
    codeInputRef.current[0].focus();
  }, []);

  //send request when all code inputs are filled
  useEffect(() => {
    if (
      !state.requestSent &&
      state.code0 !== '' &&
      state.code1 !== '' &&
      state.code2 !== '' &&
      state.code3 !== '' &&
      state.code4 !== '' &&
      state.code5 !== ''
    ) {
      setTimeout(() => {
        verifyNumber();
      }, 500);
    }
  }, [state]);

  //paste code from clipboard
  const onPaste = (e) => {
    const value = e.clipboardData.getData('Text');
    if (value.length === 6) {
      const splitCode = value.split('');

      splitCode.forEach((value, key) => {
        const code = `code${key}`;
        updateState({
          [code]: value,
        });
      });
    }
  };

  const verifyNumber = async () => {
    updateState({
      isError: false,
      statusMsg: 'Checking...',
      isChecking: true,
      requestSent: true,
    });
    //APPLY the API code validation here
    const fullCode =
      state.code0 + state.code1 + state.code2 + state.code3 + state.code4 + state.code5;
    console.log('full code:', fullCode);
    try {
      const url = props.type === 'Email' ? 'email' : 'phone';
      const response = await axios.post(`/api/${url}-verification`, { code: fullCode });
      if (response.data.success) {
        updateState({
          isError: false,
          isValid: true,
          isChecking: false,
          statusMsg: 'Phone Number Verified!',
        });
        setTimeout(() => {
          pushToNextStep();
        }, 1000);
      } else {
        updateState({
          code0: '',
          code1: '',
          code2: '',
          code3: '',
          code4: '',
          code5: '',
          isChecking: false,
          isLoading: false,
          statusMsg: 'Invalid verification code',
          isError: true,
          isValid: false,
          requestSent: false,
        });
      }
    } catch (e) {
      updateState({
        isError: true,
        isValid: false,
        isChecking: false,
        statusMsg: "Phone couldn't be verified. Try again.",
      });
    }
  };

  //push to next step
  const pushToNextStep = () => {
    if (props.type === 'Email') {
      history.push('/payment');
    } else {
      history.push('/create-account/verify-email/');
    }
  };

  //switch verification step. should be replaced with redirect
  const changePhoneEmail = () => {
    if (props.type === 'Phone') {
      history.push('/create-account/change-phone-number');
    } else {
      history.push('/create-account/change-email');
    }
  };

  //onChange and handleFocus for code input value
  const handleChangeInput = (i, e) => {
    updateState({
      [`code${i}`]: e.target.value,
    });
    if (i <= 4) {
      codeInputRef.current[i + 1].focus();
    }
  };

  return (
    <InternalSimple>
      <div className="two-step-verification">
        {!state.isLoading ? (
          <div className="center-content">
            <div className="two-step-verification__header">
              <h1 className="two-step-verification__header--title">{props.title}</h1>
              {props.type === 'Phone' ? (
                <p className={!state.isChecking ? '' : 'hide'}>
                  Text message with your verification code has <br />
                  been sent to the phone number (***)*****-
                  <span className="last-number">{state.lastNumber}</span>
                </p>
              ) : (
                <p className={!state.isChecking ? '' : 'hide'}>
                  An email with your verification code has been sent to
                  <span className="last-number"> {Validator.constHideEmail(state.userEmail)}</span>
                </p>
              )}
            </div>
            <form id="step2form" action="" className="two-step-verification__form">
              <p>{state.statusMsg}</p>
              <div id="check2step" className={`center-row ${state.isValid ? 'form-valid' : ''}`}>
                {fields.map((field, i) => {
                  return (
                    <input
                      key={i}
                      ref={(input) => {
                        codeInputRef.current[i] = input;
                      }}
                      id={i}
                      type="text"
                      className={`input-check ${state.isError ? 'input-error' : ''}`}
                      name={field.name}
                      maxLength="1"
                      autoComplete="off"
                      pattern="\d{1}"
                      value={state[field.name]}
                      onPaste={(e) => {
                        onPaste(e);
                      }}
                      onChange={(e) => handleChangeInput(i, e)}
                      disabled={state.isChecking}
                      onFocus={(e) => e.target.select()}
                    />
                  );
                })}
                <div className={`loading-gray ${state.isChecking ? '' : 'hide'}`}>
                  <img src={loadingGray} alt="Loading" />
                </div>
                <div className={`verified-icon ${state.isValid ? '' : 'hide'}`}>
                  <i className="fa fa-check" aria-hidden="true"></i>
                </div>
              </div>
              <div className={`error-msg ${state.isError ? '' : 'hide'}`}>Invalid code</div>
            </form>

            <div className={`issue-box ${!state.isChecking ? '' : 'hide'}`}>
              <p>
                Didn't receive {props.type === 'Phone' ? 'message' : 'email'} or issues with the
                code?
              </p>
              <LinkButton>Send me a new code</LinkButton>
              <div className="separator-2lines">
                <p>Or</p>
              </div>
              {props.type === 'Phone' ? (
                <LinkButton onClick={changePhoneEmail}>Try another number</LinkButton>
              ) : (
                <LinkButton onClick={changePhoneEmail}>Try another email</LinkButton>
              )}
            </div>
          </div>
        ) : (
          <div className="two-step-verification__loading">
            <LoadingStep msg="Creating your account" />
          </div>
        )}
      </div>
    </InternalSimple>
  );
}

TwoStepVerification.propTypes = {
  title: PropTypes.string.isRequired,
  type: PropTypes.oneOf(['Phone', 'Email']),
  lastNumber: PropTypes.oneOfType([PropTypes.string, PropTypes.number]),
};

TwoStepVerification.defaultProps = {
  title: 'Verify your phone number',
  type: 'Phone',
  statusMsg: 'Verification Code',
};

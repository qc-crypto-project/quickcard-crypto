import React from 'react';
import './ConfirmationDeposity.style.scss';

import { Button } from 'greenbox_ui/src/components';
import PaymentInfo from '../PaymentInfo/PaymentInfo';

import deposityImage from '@assets/Images/deposity.svg';

function ConfirmationDeposity({ confirmation }) {
  const handleSaveWallet = () => {
    window.location.href = '/save-wallet';
  };

  const handlePayMerchant = () => {
    window.location.href = '/pay-merchant';
  };

  return (
    <div className="funds-deposity">
      <div className="funds-deposity__header">
        <img alt="Funds deposity" className="funds-deposity__header__image" src={deposityImage} />
        <h2 className="funds-deposity__header__title">
          {confirmation
            ? 'Saved to Your Wallet'
            : 'We successfully charged your card and loaded your wallet'}
        </h2>
        <p className="funds-deposity__header__message">
          {confirmation
            ? null
            : 'This transaction will be reflected on your card statement as [DAMDescriptor]'}
        </p>
      </div>

      <div className="divisor" />

      <div className="funds-deposity__confirmation">
        <h5 className="funds-deposity__confirmation__title">
          {confirmation ? 'Your Confirmation' : 'Your Deposit Confirmation'}
        </h5>
        <div className="funds-deposity__confirmation__infos">
          <PaymentInfo info="Ola mundo" value="weoigesf" />
          <PaymentInfo info="Ola mundo" value="weoigesf" copy />
          <PaymentInfo info="Ola mundo" value="weqwfwafwaoigesf" copy />
        </div>
      </div>

      {confirmation ? (
        <>
          <div className="divisor" />
          <div className="funds-deposity__redirect">
            Please wait, we are redirecting you back to www.facebook.com. If you don't get
            redirected in [10] seconds, please
            <a href="http://www.facebook.com" className="funds-deposity__redirect__click">
              Click here.
            </a>
          </div>
        </>
      ) : (
        <>
          <div className="funds-deposity__buttons">
            <div className="funds-deposity__buttons__button">
              <Button variation="primary" onClick={handlePayMerchant}>
                Pay Merchant
              </Button>
            </div>

            <div className="divisor-or">
              <div className="divisor-or__line" />
              <span className="divisor-or__message">OR</span>
              <div className="divisor-or__line" />
            </div>

            <div className="funds-deposity__buttons__button">
              <Button variation="secondary" onClick={handleSaveWallet}>
                Save to my wallet
              </Button>
            </div>
          </div>
        </>
      )}
    </div>
  );
}

export default ConfirmationDeposity;

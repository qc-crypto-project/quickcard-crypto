import React, { useState, useEffect } from 'react';
import axios from 'axios';

import './InternalHeader.style.scss';

/**
 * Renders an internal header containing information about user, account number and balance
 * @param {object} props
 * @param {string} props.user Username
 * @param {string} props.account Account number (may include non-numeric characters)
 * @param {number} props.balance Remaining account balance
 */

export default function InternalHeader(props) {
  const [isLoading, setIsLoading] = useState(false);
  const [data, setData] = useState({
    user: props.user || '',
    account: props.account || '',
    balance: props.balance || 0,
  });

  //api request mock
  useEffect(() => {
    const getData = async () => {
      try {
        const response = await axios.get('api/user/admin');
        setData({
          user: response.data.user,
          account: response.data.account,
          balance: response.data.balance,
        });
      } catch (e) {
        console.log(e);
        setData({ user: '-', account: '-', balance: 0 });
      }
      setIsLoading(false);
    };

    if (!props.user) {
      setIsLoading(true);
      getData();
    }
  }, []);

  return (
    <div className="internal-header__container">
      <div className="internal-header__left">
        <h1 className="internal-header__container__title">
          Hello, <span className="internal-header__bolder">{!isLoading && data.user}</span>
        </h1>
        <small className="internal-header__container__account-number">
          Account {!isLoading && data.account}
        </small>
      </div>
      <div className="internal-header__right">
        <label className="internal-header__container__balance-label">Total Balance</label>
        <h1 className="internal-header__container__title">
          <span className="internal-header__bolder">
            {!isLoading && data.balance.toFixed(2)} QUSD
          </span>
        </h1>
      </div>
    </div>
  );
}

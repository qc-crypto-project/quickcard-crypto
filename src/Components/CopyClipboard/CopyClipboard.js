import React from 'react';

import './CopyClipboard.style.scss';

import copyImg from '@assets/Images/copy.svg';

function CopyClipboard({ message }) {
  const CopyToClipboard = async () => {
    await navigator.clipboard.writeText(message);
  };

  return (
    <>
      <div className="copy-clipboard">
        <img alt="" onClick={CopyToClipboard} src={copyImg} />
      </div>
    </>
  );
}

export default CopyClipboard;

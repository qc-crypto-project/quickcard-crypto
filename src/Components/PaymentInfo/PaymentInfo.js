import React from 'react';
import CopyClipboard from '../CopyClipboard/CopyClipboard';
import "./PaymentInfo.style.scss"


function PaymentInfo({info, value, copy}) {
    return(
      <div className="info">
        <span className="info__info">{info}</span>
        <div className="info__value">
          <span className="info__value__content">{value}</span>
          {
            copy ?
              <div className="info__value__copy">
                <CopyClipboard message={value} />
              </div>
              :
              null
          }
        </div>
      </div>
    )
}

export default PaymentInfo;
import React from 'react';
import PropTypes from 'prop-types';

import './SaveWalletModal.style.scss';

import { Button, TextButton } from 'greenbox_ui/src/components';

function SaveWalletModal({ value, onClickYes, onClickNo }) {
  return (
    <div className="background-modal">
      <div className="wallet-modal">
        <div className="wallet-modal__header">
          Are you sure you want to save the amount to your wallet?
        </div>
        <div className="wallet-modal__message">
          <p>
            The amount of
            <span className="wallet-modal__message__value"> {value} </span>
            will be saved to your wallet, and you will be able to use in another transaction
          </p>
        </div>
        <div className="wallet-modal__buttons">
          <TextButton variation="secondary" onClick={onClickNo}>
            No
          </TextButton>
          <Button variation="primary" onClick={onClickYes}>
            Yes
          </Button>
        </div>
      </div>
    </div>
  );
}

SaveWalletModal.propTypes = {
  value: PropTypes.string,
};

export default SaveWalletModal;

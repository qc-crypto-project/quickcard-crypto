import React, { useContext, useState } from 'react';
import Context from '../../Services/Context/Context';
import * as Yup from 'yup';

import './PaymentForm.style.scss';

import { Checkbox, Button, CustomSelect, Label } from 'greenbox_ui/src/components';
import { Formik } from 'formik';
import InputFormik from '../InputFormik/InputFormik';

function PaymentForm() {
  const [billingEqualMailing, setBillingEqualMailing] = useState(false);
  const { setPaymentInfos } = useContext(Context);

  const verifyFilledInfos = (values) => {
    // Check all fields filled
    console.log(values);

    const allFilled = true;

    if (allFilled) {
      handlePayment();
    }
  };

  const handlePayment = async () => {
    setPaymentInfos({
      total: '500.00 QUSD',
      walletId: 'C-35532983',
      transactionId: 'fadf7c123312...',
      purchaseId: '817293sdfg21...',
    });

    window.location.href = '/deposity';
  };

  const mockList_CustomSelect = [
    { id: 1, name: 'Option 1' },
    { id: 2, name: 'Option 2' },
    { id: 3, name: 'Option 3' },
    { id: 4, name: 'Option 4' },
  ];

  const paymentValidation = Yup.object().shape({
    cardNumber: Yup.string()
      .required('Card number is required')
      .min(16, 'Card number must have 16 characters')
      .max(16, 'Card number must have 16 characters'),
    nameCard: Yup.string()
      .min(4, 'Name must have more than 4 characters')
      .required('Name is required'),
    expiryDate: Yup.string()
      .min(4, 'Expire date must have 4 characters')
      .max(6, 'Expire date must have 4 or 6 characters')
      .required('Expire date is required'),
    cvc: Yup.string()
      .min(3, 'Cvc must have more than 2 characters')
      .max(4, 'Cvc must have less than 5 characters')
      .required('Cvc is required'),
    mailing_address1: Yup.string().required('Mailing address 1 is required'),
    mailing_city: Yup.string().required('Mailing city is required'),
    mailing_address2: Yup.string(),
    mailing_zip: Yup.string().required('Mailing zip is required'),
    billing_address1: Yup.string(),
    billing_city: Yup.string(),
    billing_address2: Yup.string(),
    billing_zip: Yup.string(),
    birthdayDate: Yup.string()
      .min(6, 'Birthday date must have more than 5 characters')
      .max(8, 'Birthday date must have lest than 9 characters')
      .required('Birthday date is required'),
    identificationNumber: Yup.string().required('Identification number is required'),
  });

  return (
    <Formik
      initialValues={{
        cardNumber: '',
        nameCard: '',
        expiryDate: '',
        cvc: '',
        mailing_address1: '',
        mailing_address2: '',
        mailing_city: '',
        mailing_zip: '',
        billing_address1: '',
        billing_address2: '',
        billing_city: '',
        billing_zip: '',
        identificationNumber: '',
        birthdayDate: '',
      }}
      validationSchema={paymentValidation}
    >
      {({ errors, touched, handleSubmit, values }) => (
        <form onSubmit={handleSubmit} className="payment-form">
          <div className="payment-form__card">
            <div className="header">
              <Label variation="black">Payment Information</Label>
              <div className="divisor" />
            </div>
            <div className="card-infos">
              <div className="card-infos__left">
                <div className="custom-input">
                  <InputFormik
                    name="cardNumber"
                    label="Card Number"
                    placeholder="**** **** **** ****"
                    variation="text"
                  />
                  {errors.cardNumber && touched.cardNumber ? (
                    <span className="error-feedback">{errors.cardNumber}</span>
                  ) : null}
                </div>

                <div className="custom-input">
                  <InputFormik
                    name="nameCard"
                    label="Name on Card"
                    placeholder="Type Name on Card"
                    variation="text"
                  />
                  {errors.nameCard && touched.nameCard ? (
                    <span className="error-feedback">{errors.nameCard}</span>
                  ) : null}
                </div>
              </div>
              <div className="card-infos__right">
                <div className="card-infos__right__checkbox">
                  <Checkbox checked />
                  <span className="card-infos__right__checkbox__message">
                    Save this card for future transactions.
                  </span>
                </div>
                <div className="card-infos__right__expire-cvc">
                  <div className="custom-input">
                    <InputFormik
                      name="expireDate"
                      label="Expiry Date"
                      placeholder="MM/YY"
                      variation="text"
                    />
                    {errors.expiryDate && touched.expiryDate ? (
                      <span className="error-feedback">{errors.expiryDate}</span>
                    ) : null}
                  </div>
                  <div className="custom-input">
                    <InputFormik name="cvc" label="CVC" placeholder="***" variation="text" />
                    <br />
                    {errors.cvc && touched.cvc ? (
                      <span className="error-feedback">{errors.cvc}</span>
                    ) : null}
                  </div>
                </div>
              </div>
            </div>
          </div>

          <div className="payment-form__mailing-address">
            <div className="header">
              <Label variation="black"> Mailing Address </Label>
              <div className="divisor" />
            </div>
            <div className="mailing-address">
              <div className="mailing-address__left">
                <div className="select-custom2">
                  <CustomSelect
                    onChange={() => console.log('On change passed')}
                    label="Country"
                    defaultText="Select Country"
                    optionsList={mockList_CustomSelect}
                  />
                </div>

                <div className="custom-input">
                  <InputFormik
                    name="mailing_address1"
                    label="Address Line 1"
                    message={`${
                      errors.mailing_address1 && touched.mailing_address1
                        ? ''
                        : 'Stret address, P.O. box'
                    }`}
                    placeholder="Enter Address"
                    variation="text"
                  />
                  {errors.mailing_address1 && touched.mailing_address1 ? (
                    <span className="error-feedback">{errors.mailing_address1}</span>
                  ) : null}
                </div>

                <div className="custom-input">
                  <InputFormik
                    name="mailing_city"
                    label="City"
                    placeholder="Enter City"
                    variation="text"
                  />
                  {errors.mailing_city && touched.mailing_city ? (
                    <span className="error-feedback">{errors.mailing_city}</span>
                  ) : null}
                </div>
              </div>
              <div className="mailing-address__right">
                <div className="select-custom2">
                  <CustomSelect
                    onChange={() => console.log('On change passed')}
                    label="State/Province/Region"
                    defaultText="Select"
                    optionsList={mockList_CustomSelect}
                  />
                </div>
                <div className="custom-input">
                  <InputFormik
                    name="mailing_address2"
                    label="Address Line 2"
                    optional
                    message={`${
                      errors.mailing_address2 && touched.mailing_address2
                        ? ''
                        : 'Apartment, suite, unit, building, floor, etc.'
                    }`}
                    placeholder="Enter Address"
                    variation="text"
                  />
                  {errors.mailing_address2 && touched.mailing_address2 ? (
                    <span className="error-feedback">{errors.mailing_address2}</span>
                  ) : null}
                </div>
                <div className="custom-input">
                  <InputFormik
                    name="mailing_zip"
                    label="Zip/Postal Code"
                    placeholder="000"
                    variation="text"
                  />
                  {errors.mailing_zip && touched.mailing_zip ? (
                    <span className="error-feedback">{errors.mailing_zip}</span>
                  ) : null}
                </div>
              </div>
            </div>
          </div>

          <div className="payment-form__billing-address">
            <div className="header">
              <Label variation="black"> Billing Address </Label>
              <div className="header__checkbox">
                <Checkbox onChange={() => setBillingEqualMailing(!billingEqualMailing)} />
                <span className="header__checkbox__message"> Same as Mailing Address </span>
              </div>
            </div>
            <div className="divisor" />
            {billingEqualMailing ? null : (
              <div className="billing-address">
                <div className="billing-address__left">
                  <div className="select-custom2">
                    <CustomSelect
                      onChange={() => console.log('On change passed')}
                      label="Country"
                      defaultText="Select Country"
                      optionsList={mockList_CustomSelect}
                    />
                  </div>

                  <div className="custom-input">
                    <InputFormik
                      name="billing_address1"
                      label="Address Line 1"
                      placeholder="Enter Address"
                      variation="text"
                    />
                    {errors.billing_address1 && touched.billing_address1 ? (
                      <span className="error-feedback">{errors.billing_address1}</span>
                    ) : null}
                  </div>

                  <div className="custom-input">
                    <InputFormik
                      name="billing_city"
                      label="City"
                      placeholder="Enter City"
                      variation="text"
                    />

                    {errors.billing_address1 && touched.billing_address1 ? (
                      <span className="error-feedback">{errors.billing_address1}</span>
                    ) : null}
                  </div>
                </div>
                <div className="billing-address__right">
                  <div className="select-custom2">
                    <CustomSelect
                      onChange={() => console.log('On change passed')}
                      label="State/Province/Region"
                      defaultText="Select"
                      optionsList={mockList_CustomSelect}
                    />
                  </div>

                  <div className="custom-input">
                    <InputFormik
                      name="billing_address2"
                      label="Address Line 2"
                      placeholder="Enter Address"
                      variation="text"
                    />
                    {errors.billing_address2 && touched.billing_address2 ? (
                      <span className="error-feedback">{errors.billing_address2}</span>
                    ) : null}
                  </div>
                  <div className="custom-input">
                    <InputFormik
                      name="billing_zip"
                      label="Zip/Postal Code"
                      placeholder="000"
                      variation="text"
                    />

                    {errors.billing_zip && touched.billing_zip ? (
                      <span className="error-feedback">{errors.billing_zip}</span>
                    ) : null}
                  </div>
                </div>
              </div>
            )}
          </div>
          <div className="payment-form__identification">
            <div className="header">
              <Label variation="black"> Identification </Label>
              <div className="divisor" />
            </div>
            <div className="identification">
              <div className="identification__left">
                <div className="select-custom2">
                  <CustomSelect
                    onChange={() => console.log('On change passed')}
                    label="Identification Type"
                    defaultText="Select Document"
                    optionsList={mockList_CustomSelect}
                  />
                </div>

                <div className="custom-input">
                  <InputFormik
                    name="birthdayDate"
                    label="Date of Birth"
                    placeholder="09/02/1988"
                    variation="text"
                  />
                  {errors.birthdayDate && touched.birthdayDate ? (
                    <span className="error-feedback">{errors.birthdayDate}</span>
                  ) : null}
                </div>
              </div>
              <div className="identification__right">
                <div className="custom-input">
                  <InputFormik
                    name="identificationNumber"
                    label="Identification Number"
                    variation="text"
                  />
                  <br />
                  {errors.identificationNumber && touched.identificationNumber ? (
                    <span className="error-feedback">{errors.identificationNumber}</span>
                  ) : null}
                </div>
              </div>
            </div>
          </div>

          <div className="payment-form__pay-button">
            <Button variation="primary" disable={true}>
              Pay 520.00 GBP
            </Button>
          </div>
        </form>
      )}
    </Formik>
  );
}

export default PaymentForm;

export const ROUTES = {
  /**
   * Auth routes
   */
  ROOT: '/',
  AUTH: '/auth',
  LOGIN: '/auth/login',
  LOGOUT: '/auth/logout',
  CREATE_ACCOUNT: '/auth/signup',
  UNAUTHORIZED: '/auth/unauthorized',

  /**
   * Main Routes
   */
  HOME: '/home',
};

export const API_ROUTES = {
  login: `api/auth`,
  location: `api/locations`,
  users: `api/users`,
};

import React from 'react';
import { BrowserRouter as Router, Switch, Route } from 'react-router-dom';
import { AnimatedSwitch } from 'react-router-transition';
import { Online, Offline } from 'react-detect-offline';

import { Base } from './Components/Layouts';
import { Context } from '../src/Services/Context/Context';

import CreateAccount from './Pages/CreateAccount';
import Home from './Pages/Home';
import AddFunds from './Pages/AddFunds/AddFunds';
import Deposity from './Pages/Deposity';
import SaveWallet from './Pages/SaveWallet';
import PayMerchant from './Pages/PayMerchant';
import CardError from './Pages/CardError';
import Login from './Pages/Login';
import Cards from './Pages/Cards';

function App() {
  return (
    <>
      <Online polling={false}>
        <Context>
          <Base>
            <Router>
              <Switch>
                <AnimatedSwitch
                  atEnter={{ opacity: 0 }}
                  atLeave={{ opacity: 0 }}
                  atActive={{ opacity: 1 }}
                  className="switch-wrapper"
                >
                  <Route exact path="/" component={Home} />
                  <Route path="/login" component={Login} />
                  <Route path="/create-account" component={CreateAccount} />
                  <Route path="/payment" component={AddFunds} />
                  <Route path="/deposity" component={Deposity} />
                  <Route path="/save-wallet" component={SaveWallet} />
                  <Route path="/pay-merchant" component={PayMerchant} />
                  <Route path="/card-error" component={CardError} />
                  <Route path="/cards" component={Cards} />
                </AnimatedSwitch>
              </Switch>
            </Router>
          </Base>
        </Context>
      </Online>
      <Offline polling={false}>
        <p>OFFLINE</p>
        <Home />
      </Offline>
    </>
  );
}

export default App;

// @ts-nocheck
const { pullCommonFiles, pullTargetFiles, pullEnvFile } = require('./files');
const Config = require('./config');

/**
 * Configs require PLATFORM_TARGET and NODE_ENV to run
 */
// eslint-disable-next-line no-console
console.log(`Creating app config against ${process.env.NODE_ENV} enviornment \n`.magenta);

const config = new Config(
  process.env.NODE_ENV,
  pullCommonFiles,
  pullEnvFile
);

module.exports = config.getConfig();

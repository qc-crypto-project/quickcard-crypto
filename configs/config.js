// @ts-nocheck
const colors = require('colors');

class Config {
  constructor(environment, pullCommonFiles, pullEnvFile, errorHandler) {
    this.environment = environment;
    this.env = environment === 'production' ? 'prod' : 'dev';
    this.envPath = environment === 'production' ? '.env.prod' : '.env';
    this.config = {};
    this.error = null;
    this.errorHandler = errorHandler || this._onHardError;

    this.pullCommonFiles = pullCommonFiles;
    this.pullEnvFile = pullEnvFile;

    this.checkEnv();

    if (!this.error) {
      this.make();
    }
  }

  /**
   * Sets the internal error and stops the process from continuing.
   * @param {string} error
   */
  _onHardError(error) {
    this.error = error;

    console.log(`App config: ${error} \n`.red);
    process.exit(1);
  }

  /**
   * Checks if the required env properties are present.
   */
  checkEnv() {
    if (!this.environment) {
      return this.errorHandler('NODE_ENV is missing. Unable to make config.');
    }
  }

  /**
   * Processes the requires schema and applies found properties.
   * @param {string} type
   * @param {*} files
   * @param {*} env
   */
  _processRequired(type, files, env) {
    if (!files || !files.schema) {
      this.errorHandler(`No required schema declaration for the ${type} config.`);

      return;
    }

    if (files.schema.required) {
      for (const key of files.schema.required) {
        this._checkRequiredKeys(key, env, files, type);
      }
    }
  }

  /**
   * Checks to see if the property is found in the process, .env or the config file.
   * @param {string} key
   * @param {*} env
   * @param {*} files
   * @param {*} type
   */
  _checkRequiredKeys(key, env, files, type) {
    if (key in process.env) {
      this.config[key] = process.env[key];
    } else if (env && env.parsed && key in env.parsed) {
      this.config[key] = env.parsed[key];
    } else if (key in files.config) {
      this.config[key] = files.config[key];
    } else {
      this.errorHandler(`Missing ${key} property in ${type} config`);
    }
  }

  /**
   * Processes the optional schema and applies found properties.
   * @param {*} files
   * @param {*} env
   */
  _processOptional(files, env) {
    if (!files || !files.schema || !files.schema.optional) {
      return;
    }

    for (const key of files.schema.optional) {
      if (typeof key === 'string') {
        this._checkOptionalKeys(key, env, files);
      } else {
        this._checkKeys(key, env);
      }
    }
  }

  _checkKeyInInput(keys, input) {
    for (const key of keys) {
      if (key in input) {
        return key;
      }
    }
  }

  /**
   * Checks to see if the property is found in process, .env or config file.
   * @param {string} key
   * @param {*} env
   * @param {*} files
   */
  _checkOptionalKeys(key, env, files) {
    if (key in process.env) {
      this.config[key] = process.env[key];
    } else if (env && key in env.parsed) {
      this.config[key] = env.parsed[key];
    } else if (key in files.config) {
      this.config[key] = files.config[key];
    }
  }

  _checkKeys(key, env) {
    const keys = key.in;
    const env_key = env ? this._checkKeyInInput(keys, env.parsed) : null;
    const process_key = this._checkKeyInInput(keys, process.env);

    if (env_key) {
      this.config[key.out] = env.parsed[env_key];
    }

    if (process_key) {
      this.config[key.out] = process.env[process_key];
    }
  }

  /**
   * Processes the entire config based on the target and env.
   */
  make() {
    const common = this.pullCommonFiles(this.env);
    const env = this.pullEnvFile(this.envPath);

    this._processRequired('common', common, env);

    this._processOptional(common, env);
  }

  getConfig() {
    return this.config;
  }
}

module.exports = Config;

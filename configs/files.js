const colors = require('colors');
const jetpack = require('fs-jetpack');

const pullCommonFiles = (env) => {
  const files = {
    config: require(`./common.${env}.json`),
    schema: require(`./schema.json`),
  };

  return files;
};

const pullEnvFile = (path) => {
  if (jetpack.exists(path)) {
    const dotenv = require('dotenv').config({ path });

    if (dotenv.error) {
      this.hardNo(dotenv.error);
    } else {
      return dotenv;
    }
  } else {
    // @ts-ignore
    console.log(`App config: running without ${path} file \n`.magenta);
  }
};

module.exports = { pullCommonFiles, pullEnvFile };

module.exports = {
  env: {
    browser: true,
    es6: true,
    'jest/globals': true
  },
  parser: "@typescript-eslint/parser", // Specifies the ESLint parser
  parserOptions: {
    ecmaVersion: 2020, // Allows for the parsing of modern ECMAScript features
    sourceType: "module", // Allows for the use of imports
    ecmaFeatures: {
      jsx: true // Allows for the parsing of JSX
    }
  },
  settings: {
    react: {
      version: "detect" // Tells eslint-plugin-react to automatically detect the version of React to use
    },
    "import/resolver": {
      "node": {
        "extensions": [".js", ".jsx", ".ts", ".tsx"]
      }
    }
  },
  plugins: [
    "react-hooks",
  ],
  extends: [
    "plugin:import/errors",
    "plugin:import/warnings",
    "plugin:import/typescript",
    "plugin:react/recommended", // Uses the recommended rules from @eslint-plugin-react
    "plugin:@typescript-eslint/recommended", // Uses the recommended rules from the @typescript-eslint/eslint-plugin
    "prettier/@typescript-eslint", // Uses eslint-config-prettier to disable ESLint rules from @typescript-eslint/eslint-plugin that would conflict with prettier
    'airbnb',
    'plugin:jest/recommended',
    "plugin:prettier/recommended" // Enables eslint-plugin-prettier and eslint-config-prettier. This will display prettier errors as ESLint errors. Make sure this is always the last configuration in the extends array.
  ],
  rules: {
    "max-len": ["error", { "code": 120}],
    "react/prop-types": "off",
    "no-unused-vars": "off",
    "no-unused-expressions": "off",
    "@typescript-eslint/no-unused-expressions": "error",
    "@typescript-eslint/no-unused-vars": [
      "error",
      {
        "argsIgnorePattern": "^_"
      }
    ],
    "import/prefer-default-export": "off",
    "react/jsx-wrap-multilines": ["error", {"declaration": false, "assignment": false}],
    "react/jsx-filename-extension": [2, { 'extensions': ['.js', '.jsx', '.ts', '.tsx'] } ],
    "import/extensions": [2, { "js": "never", "jsx": "never", "ts": "never", "tsx": "never", 'json': 'never' }],
    "react/jsx-props-no-spreading": "off",
    "jsx-a11y/anchor-is-valid": "off",
    "jsx-a11y/mouse-events-have-key-events": "off",
    "react/jsx-fragments": "off",
    "react/jsx-one-expression-per-line": "off",
    "react-hooks/rules-of-hooks": "error",
    "react-hooks/exhaustive-deps": "warn",

    // allow dev dependencies in stories and tests
    'import/no-extraneous-dependencies': [
      'error',
      {
        devDependencies: [
          '/.storybook/**',
          '**/*.test.@(js|jsx|ts|tsx)',
          '**/*.stories.@(js|jsx|ts|tsx|mdx)',
        ],
      },
    ],
  },
  overrides: [
    {
      "files": ["configs/*.js"],
    },
  ]
};

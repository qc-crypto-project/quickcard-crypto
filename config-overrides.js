const { alias } = require('react-app-rewire-alias');
const {
  override,
  addDecoratorsLegacy,
  disableEsLint,
  addExternalBabelPlugins,
} = require('customize-cra');

function myOverrides(config) {
  alias({
    '@styles': './src/Styles',
    '@components': './src/Components',
    '@assets': './src/Assets',
    '@services': './src/Services',
    '@pages': './src/Pages',
  })(config);
  return config;
}

module.exports = override(
  myOverrides,
  addDecoratorsLegacy(),
  disableEsLint(),
  ...addExternalBabelPlugins(
    '@babel/plugin-transform-react-jsx',
    '@babel/plugin-proposal-class-properties'
  )
);
